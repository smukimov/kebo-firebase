'use strict';

const functions = require('firebase-functions');
const mkdirp = require('mkdirp-promise');
// Include a Service Account Key to use a Signed URL
const gcs = require('@google-cloud/storage')({keyFilename: 'service-account-credentials.json'});
const Vision = require('@google-cloud/vision');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');
const sharp = require('sharp');

/**
 * When an image is uploaded in the Storage bucket We recognize a face automatically using
 * Vision and crop it using ImageMagick.
 * After the face has been recognized and uploaded to Cloud Storage,
 * we write the public URL to the Firebase Realtime Database.
 */
exports.generateFace = functions.storage.bucket('kebo-93097-faces').object().onChange(event => {
  // Max height and width of the face in pixels.
  const FACE_MAX_HEIGHT = 200;
  const FACE_MAX_WIDTH = 200;
  // face prefix added to file names.
  const FACE_PREFIX = 'face_';
  // File and directory paths.
  const filePath = event.data.name;
  const fileDir = path.dirname(filePath);
  const fileName = path.basename(filePath);
  const [uid, uuid] = fileDir.split('/');
  const faceFilePath = path.normalize(path.join(uid, uuid, `${FACE_PREFIX}${fileName}`));
  const tempLocalFile = path.join(os.tmpdir(), uid, uuid, filePath);
  const tempLocalDir = path.dirname(tempLocalFile);
  const tempLocalFaceFile = path.join(os.tmpdir(), faceFilePath);

  console.log(JSON.stringify(event.data));

  // Exit if this is triggered on a file that is not an image.
  if (!event.data.contentType.startsWith('image/')) {
    console.log('This is not an image.');
    return;
  }

  // Exit if the image is already a face.
  if (fileName.startsWith(FACE_PREFIX)) {
    console.log('Already a face.');
    return;
  }

  // Exit if this is a move or deletion event.
  if (event.data.resourceState === 'not_exists') {
    console.log('This is a deletion event.');
    return;
  }

  // Cloud Storage files.
  const bucket = gcs.bucket(event.data.bucket);
  const file = bucket.file(filePath);
  const faceFile = bucket.file(faceFilePath);

  const vision = Vision();

  // Create the temp directory where the storage file will be downloaded.
  return mkdirp(tempLocalDir).then(() => {
    console.log('Download file from bucket', filePath);
    // Download file from bucket.
    return file.download({destination: tempLocalFile});
  }).then(() => {
    console.log('Face detection', tempLocalFile);
    const request = {source: {filename: tempLocalFile}};

    return vision.faceDetection(request)
      .then((results) => {
        const faces = results[0].faceAnnotations;
        const numFaces = faces.length;
        console.log('Found ' + numFaces + (numFaces === 1 ? ' face' : ' faces'));
        if (numFaces > 0) {
          return faces[0].boundingPoly.vertices[0];
        }
        return { x: 0, y: 0 };
      });
  }).then(vertex => {
    console.log('The file has been downloaded to', tempLocalFile);
    // Generate a face using ImageMagick.
    return spawn('convert', [tempLocalFile, '-crop', `${FACE_MAX_WIDTH}x${FACE_MAX_HEIGHT}+${vertex.x-20}+${vertex.y-20}`, tempLocalFaceFile]);
  }).then(() => {
    console.log('face created at', tempLocalFaceFile);
    // Uploading the face.
    return Promise.all([
      bucket.upload(tempLocalFaceFile, {destination: faceFilePath})
    ]);
  }).then(() => {
    console.log('set metadata');
    return faceFile.setMetadata({
      metadata: {
        origPath: filePath
      }
    });
  }).then(() => {
    console.log('update user profile');
    // Once the image has been uploaded delete the local files to free up disk space.
    fs.unlinkSync(tempLocalFile);
    fs.unlinkSync(tempLocalFaceFile);

    const updates = {
      [`/user-profile/${uid}/photoURL`]: faceFilePath
    };
    return admin.database().ref().update(updates);
  }).catch((err) => {
    console.error('ERROR:', err);
  });
});

/**
 * When an image is uploaded in the Storage bucket We generate a thumbnail automatically using
 * Sharp.
 */
exports.generateThumbnail = functions.storage.bucket('kebo-93097-logos').object().onChange(event => {
  const THUMB_MAX_WIDTH = 256;
  const THUMB_MAX_HEIGHT = 256;

  const object = event.data; // The Storage object.
  const fileBucket = object.bucket; // The Storage bucket that contains the file.
  const filePath = object.name; // File path in the bucket.
  const fileDir = path.dirname(filePath);
  const [cid] = fileDir.split('/');
  const contentType = object.contentType; // File content type.
  const resourceState = object.resourceState; // The resourceState is 'exists' or 'not_exists' (for file/folder deletions).
  const metageneration = object.metageneration; // Number of times metadata has been generated. New objects have a value of 1.

  // Exit if this is triggered on a file that is not an image.
  if (!contentType.startsWith('image/')) {
    console.log('This is not an image.');
    return;
  }

  // Get the file name.
  const fileName = path.basename(filePath);
  // Exit if the image is already a thumbnail.
  if (fileName.startsWith('thumb_')) {
    console.log('Already a Thumbnail.');
    return;
  }

  // Exit if this is a move or deletion event.
  if (resourceState === 'not_exists') {
    console.log('This is a deletion event.');
    return;
  }

  // Exit if file exists but is not new and is only being triggered
  // because of a metadata change.
  if (resourceState === 'exists' && metageneration > 1) {
    console.log('This is a metadata change event.');
    return;
  }

  // Download file from bucket.
  const bucket = gcs.bucket(fileBucket);

  const metadata = {
    contentType: contentType
  };
  // We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
  const thumbFileName = `thumb_${fileName}`;
  const thumbFilePath = path.join(path.dirname(filePath), thumbFileName);
	const thumbnailUploadFile = bucket.file(thumbFilePath);
	console.log(thumbFilePath);
  // Create write stream for uploading thumbnail
  const thumbnailUploadStream = thumbnailUploadFile.createWriteStream({metadata});

  // Create Sharp pipeline for resizing the image and use pipe to read from bucket read stream
  const pipeline = sharp();
  pipeline
    .resize(THUMB_MAX_WIDTH, THUMB_MAX_HEIGHT)
    .ignoreAspectRatio()
    .crop(sharp.strategy.entropy)
    .pipe(thumbnailUploadStream);

  bucket.file(filePath).createReadStream().pipe(pipeline);

  const streamAsPromise = new Promise((resolve, reject) => thumbnailUploadStream.on('finish', resolve).on('error', reject));

  return streamAsPromise.then(() => {
    console.log('Thumbnail created successfully');
    const updates = {
			[`/chats/${cid}/logoURL`]: thumbFilePath,
    };
    return admin.database().ref().update(updates);
  });
});

// Authenticate to Algolia Database.
// TODO: Make sure you configure the `algolia.app_id` and `algolia.api_key` Google Cloud environment variables.
const algoliasearch = require('algoliasearch');
const client = algoliasearch(functions.config().algolia.app_id, functions.config().algolia.api_key);

// Updates the search index when new blog entries are created or updated.
exports.indexentry = functions.database.ref('/chats/{chatId}').onWrite(event => {
	const index = client.initIndex('chats');
	const chat = event.data.val()
	const firebaseObject = {
		name: chat.name,
		logoURL: chat.logoURL,
		objectID: event.params.chatId,
	};

	return chat.isPrivate ? null : index.saveObject(firebaseObject);
});

