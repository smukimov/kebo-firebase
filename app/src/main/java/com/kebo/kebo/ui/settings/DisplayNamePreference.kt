package com.kebo.kebo.ui.settings

import android.content.Context
import android.support.v7.preference.DialogPreference
import android.util.AttributeSet

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.kebo.kebo.entities.Constants
import com.kebo.kebo.R

class DisplayNamePreference : DialogPreference {
    private val mDialogLayoutResId = R.layout.pref_dialog_display_name

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int)
            : super(context, attrs, defStyleAttr, defStyleRes)

    constructor(context: Context, attrs: AttributeSet,
                @Suppress("UNUSED_PARAMETER") defStyleAttr: Int)
            : super(context, attrs, R.attr.preferenceStyle)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context) : super(context)

    override fun getDialogLayoutResource(): Int = mDialogLayoutResId

    var displayName: String?
        get() = FirebaseAuth.getInstance().currentUser!!.displayName
        set(displayName) {
            FirebaseDatabase.getInstance().reference
                    .child(Constants.USER_PROFILE)
                    .child(FirebaseAuth.getInstance().currentUser!!.uid)
                    .child("displayName")
                    .setValue(displayName)
        }
}
