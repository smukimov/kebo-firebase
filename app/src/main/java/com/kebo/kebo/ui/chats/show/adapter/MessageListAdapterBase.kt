package com.kebo.kebo.ui.chats.show.adapter

import android.support.v7.widget.RecyclerView
import com.firebase.ui.database.FirebaseArray
import com.kebo.kebo.entities.Message
import com.kebo.kebo.entities.MessageType
import com.kebo.kebo.models.interactors.MessageInfo
import com.kebo.kebo.ui.global.RecyclerAdapterChangeListener
import com.kebo.kebo.ui.global.viewholder.BaseViewHolder
import kotlinx.android.synthetic.main.item_message.view.*

abstract class MessageListAdapterBase(private val messageList: FirebaseArray<Message>)
    : RecyclerAdapterChangeListener() {

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val mes = getItem(position)
        @Suppress("UNCHECKED_CAST")
        val messageViewHolder = viewHolder as BaseViewHolder<MessageInfo>
        messageViewHolder.bind(mes)
        messageViewHolder.itemView.nickTV.setOnClickListener({
            onAvatarClick(mes)
        })
    }

    override fun startListening() {
        if (!messageList.isListening(this)) {
            messageList.addChangeEventListener(this)
        }
    }

    override fun cleanup() {
        notifyItemRangeRemoved(0, itemCount)
        messageList.removeChangeEventListener(this)
    }

    fun getItem(position: Int) = MessageInfo(messageList.getObject(position))

    override fun getItemCount() = if (messageList.isListening(this)) messageList.size else 0

    override fun getItemViewType(position: Int): Int {
        val message = getItem(position)
        return if (message.hasImageUrl()) MessageType.IMAGE.ordinal else MessageType.TEXT.ordinal
    }

    abstract fun onAvatarClick(messageInfo: MessageInfo)
}