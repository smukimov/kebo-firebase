package com.kebo.kebo.ui.drawer

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.kebo.kebo.R
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.extension.visible
import com.kebo.kebo.presentation.drawer.NavigationDrawerContract
import com.kebo.kebo.presentation.drawer.NavigationDrawerContract.View.MenuItem
import com.kebo.kebo.ui.launch.MainActivity
import kotlinx.android.synthetic.main.fragment_nav_drawer.*
import kotlinx.android.synthetic.main.item_avatar.*
import javax.inject.Inject
import com.kebo.kebo.presentation.drawer.NavigationDrawerContract.View.MenuItem.*
import com.kebo.kebo.presentation.drawer.NavigationDrawerPresenter
import com.kebo.kebo.ui.global.BaseFragmentMvp

@ActivityScoped
class NavigationDrawerFragment
    @Inject constructor() : BaseFragmentMvp(), NavigationDrawerContract.View {
    @Inject lateinit var presenterProvider: dagger.Lazy<NavigationDrawerPresenter>
    @InjectPresenter lateinit var presenter: NavigationDrawerPresenter
    @ProvidePresenter fun provideNavigationDrawerPresenter(): NavigationDrawerPresenter =
            presenterProvider.get()

    private var mainActivity: MainActivity? = null

    private val itemClickListener = { view: View ->
        mainActivity?.openNavDrawer(false)
        presenter.onMenuItemClick(view.tag as MenuItem)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as MainActivity
        presenter.showUserProfile()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        chatsPrivateMI.tag = CHATS_PRIVATE
        chatsIntroMI.tag = CHATS_INTRO
        favoritesMI.tag = FAVORITES
        settingsMI.tag = SETTINGS
        signOutMI.tag = SIGN_OUT

        chatsPrivateMI.setOnClickListener(itemClickListener)
        chatsIntroMI.setOnClickListener(itemClickListener)
        favoritesMI.setOnClickListener(itemClickListener)
        settingsMI.setOnClickListener(itemClickListener)
        signOutMI.setOnClickListener(itemClickListener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_nav_drawer, container, false)

    override fun showUserInfo(userProfile: UserProfile) {
        nickTV.text = userProfile.displayName

        if (userProfile.photoURL != null) {
            Glide
                    .with(avatarIV.context)
                    .using(FirebaseImageLoader())
                    .load(userProfile.photoURLRef)
                    .asBitmap()
                    .centerCrop()
                    .into(object : BitmapImageViewTarget(avatarIV) {
                        override fun setResource(resource: Bitmap?) {
                            resource?.let {
                                avatarIV.visible(true)
                                RoundedBitmapDrawableFactory.create(view.resources, it).run {
                                    this.isCircular = true
                                    avatarIV.setImageDrawable(this)
                                }
                            }
                        }
                    })

        }
    }

    override fun onScreenChanged(menuItem: MenuItem) {
        presenter.onScreenChanged(menuItem)
    }

    override fun selectMenuItem(menuItem: MenuItem) {
        (0 until navDrawerMenuContainer.childCount)
                .map { navDrawerMenuContainer.getChildAt(it) }
                .forEach { item ->
                    item.tag?.let {
                        item.isSelected = it == menuItem
                    }
                }

    }
}