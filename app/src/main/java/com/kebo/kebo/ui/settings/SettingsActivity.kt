package com.kebo.kebo.ui.settings

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

import com.bumptech.glide.Glide
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.jakewharton.rxbinding2.view.RxView
import com.kebo.kebo.entities.Constants
import com.kebo.kebo.R
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.presentation.settings.SettingsContract
import com.kebo.kebo.presentation.settings.SettingsPresenter
import com.kebo.kebo.ui.global.BaseActivity
import com.kebo.kebo.ui.global.ActivityUtils
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_settings.*

import java.util.*
import javax.inject.Inject
import android.content.Intent
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kebo.kebo.extension.addTo
import durdinapps.rxfirebase2.RxFirebaseStorage
import io.reactivex.disposables.CompositeDisposable
import mu.KLogging

class SettingsActivity : BaseActivity(), PreferenceDialogResult, SettingsContract.View {
    @Inject lateinit var settingsFragmentProvider: dagger.Lazy<SettingsFragment>
    @Inject lateinit var presenterProvider: dagger.Lazy<SettingsPresenter>
    @InjectPresenter lateinit var presenter: SettingsPresenter
    @ProvidePresenter fun provideSettingsPresenter(): SettingsPresenter =
            presenterProvider.get()

    private val disposable = CompositeDisposable()

    override fun onResume() {
        super.onResume()
        presenter.retrieveUserProfile()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        if (savedInstanceState == null) {
            ActivityUtils.addFragmentToActivity(supportFragmentManager,
                    settingsFragmentProvider.get(), R.id.mainContainer)
        }

        setSupportActionBar(findViewById(R.id.toolbar))

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setTitle(R.string.title_activity_settings)
        }

        RxView.clicks(avatarIV)
                .subscribe {
                    requestPermission().subscribe {
                        presenter.onAvatarClicked(it)
                    }
                }.addTo(disposable)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == READ_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                val fileUri = data?.data

                if (fileUri != null) {
                    onFileSelection(fileUri)
                } else {
                    logger.warn { "File URI is null" }
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showToast(id: Int) {
        showToast(getString(id))
    }

    private fun onFileSelection(uri: Uri) {
        showProgressDialog()
        val fileRef = FirebaseStorage.getInstance("gs://kebo-93097-faces")
                .reference
                .child(FirebaseAuth.getInstance().currentUser!!.uid)
                .child(UUID.randomUUID().toString())
                .child(uri.lastPathSegment)

        RxFirebaseStorage.putFile(fileRef, uri)
                .subscribe {
                    FirebaseDatabase.getInstance().reference
                            .child(Constants.USER_PROFILE)
                            .child(FirebaseAuth.getInstance().currentUser!!.uid)
                            .child("photoURL")
                            .setValue(fileRef.path)
                            .addOnCompleteListener { hideProgressDialog() }
                }
    }


    private fun showProgressDialog() {
    }

    private fun hideProgressDialog() {
    }

    override fun showUserProfile(userProfile: UserProfile) {
        if (userProfile.photoURL is String) {
            Glide.with(this)
                    .using<StorageReference>(FirebaseImageLoader())
                    .load(userProfile.photoURLRef)
                    .into(avatarIV)
        }

        (nickTV as TextView).text = userProfile.displayName
    }

    private fun requestPermission() =
            RxPermissions(this).request(Manifest.permission.READ_EXTERNAL_STORAGE)

    override fun openFileChooserDialog() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        startActivityForResult(intent, READ_REQUEST_CODE)
    }

    private val READ_REQUEST_CODE = 42

    companion object : KLogging()
}
