package com.kebo.kebo.ui.global.navigation.menu

import android.support.v7.widget.PopupMenu
import android.util.SparseArray
import android.view.MenuItem

interface MenuAction {
    fun execute(menuItem: MenuItem)
}

class MenuActions private constructor(builder: Builder) : PopupMenu.OnMenuItemClickListener {
    private val actions: SparseArray<MenuAction>

    init {
        this.actions = builder.actions
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        val itemId = item.itemId
        actions.get(itemId).execute(item)
        return actions.indexOfKey(itemId) > 0
    }

    class Builder {
        val actions = SparseArray<MenuAction>()

        constructor()

        constructor(vararg items: MenuActionItem) {
            for (item in items) {
                action(item.id, item.action)
            }
        }

        fun action(itemId: Int, action: MenuAction): Builder {
            actions.put(itemId, action)
            return this
        }

        fun append(menuActions: MenuActions): Builder = append(menuActions.actions)

        fun append(anotherActionBuilder: MenuActions.Builder): Builder =
                append(anotherActionBuilder.actions)

        fun append(actions: SparseArray<MenuAction>): Builder {
            for (i in 0 until actions.size()) {
                this.actions.put(actions.keyAt(i), actions.valueAt(i))
            }
            return this
        }

        fun build(): MenuActions = MenuActions(this)
    }

    class MenuActionItem(val id: Int, val action: MenuAction)
}