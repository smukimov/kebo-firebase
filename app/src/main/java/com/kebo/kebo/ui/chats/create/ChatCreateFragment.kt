package com.kebo.kebo.ui.chats.create

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.jakewharton.rxbinding2.view.RxView
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.presentation.chats.create.ChatCreateContract
import com.kebo.kebo.presentation.chats.create.ChatCreatePresenter
import com.kebo.kebo.ui.global.BaseFragment
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_chat_create.*
import javax.inject.Inject

class ChatCreateFragment @Inject constructor() : BaseFragment(), ChatCreateContract.View {
    @Inject lateinit var presenterProvider: dagger.Lazy<ChatCreatePresenter>
    @InjectPresenter lateinit var presenter: ChatCreatePresenter
    @ProvidePresenter fun provideChatCreatePresenter(): ChatCreatePresenter =
            presenterProvider.get()

    private val disposables = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override val screenType: ScreenType
        get() = ScreenType.CHAT_CREATE

    override val layoutRes: Int
        get() = R.layout.fragment_chat_create

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        buildNavigation(View.OnClickListener {
            presenter.onBackPressed()
        })
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(chatCreateACB).subscribe {
            if (validateForm()) {
                collectData()
            }
        }.addTo(disposables)
    }

    private fun validateForm(): Boolean {
        nameET.error = null

        if (nameET.text.isNullOrEmpty()) {
            nameET.error = getString(R.string.error_field_required)
            return false
        }

        return true
    }

    private fun collectData() {
        presenter.create(mapOf(ChatGroupInfo.FieldName.NAME to nameET.text.toString()))
    }
}