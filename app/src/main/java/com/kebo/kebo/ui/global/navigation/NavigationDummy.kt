package com.kebo.kebo.ui.global.navigation

import android.view.View
import com.google.auto.factory.AutoFactory
import com.kebo.kebo.ui.global.navigation.constants.NavigationIconType
import com.kebo.kebo.ui.global.navigation.menu.MenuActions

@AutoFactory(implementing = arrayOf(NavigationFactory::class))
class NavigationDummy(id: Int) : Navigation(id) {

    override fun buildNavigation(onNavigationIconListener: View.OnClickListener?,
                       vararg menuActionItem: MenuActions.MenuActionItem) {

        toolbarNavigationIcon(NavigationIconType.BACK)
        toolbarNavigationIconListener = onNavigationIconListener
    }
}