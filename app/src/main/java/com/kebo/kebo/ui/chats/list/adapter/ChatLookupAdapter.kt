package com.kebo.kebo.ui.chats.list.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.kebo.kebo.R
import com.kebo.kebo.extension.inflate
import com.kebo.kebo.models.interactors.HitInfo

class ChatLookupAdapter : BaseAdapter(), Filterable {

    var results = listOf<HitInfo>()

    fun replaceData(hits: List<HitInfo>?) {
        results = hits ?: listOf()
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            view = parent!!.inflate(R.layout.item_simple_chat, false)
        }
        val item: HitInfo = getItem(position)
        view.findViewById<TextView>(R.id.list_chat_name).text = item.hit.name

        val chatLogo = view.findViewById<ImageView>(R.id.list_chat_logo)
        if (item.hasLogo()) {
            Glide.with(parent!!.context)
                    .using(FirebaseImageLoader())
                    .load(item.logoURLReference)
                    .into(chatLogo)
        } else {
            chatLogo.setImageResource(R.drawable.ic_account_circle_black_36dp)
        }
        return view
    }

    override fun getItem(index: Int): HitInfo = results[index]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = results.size

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                filterResults.values = results
                filterResults.count = results.size
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }
}