package com.kebo.kebo.ui.global.navigation.constants

enum class NavigationIconType(val type: Int) {
    BACK(0), MENU(1), NO_NAV_ICON(-1)
}