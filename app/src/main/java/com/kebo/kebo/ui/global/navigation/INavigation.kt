package com.kebo.kebo.ui.global.navigation

import android.view.View
import com.kebo.kebo.ui.global.navigation.menu.MenuActions

interface INavigation {
    fun buildNavigation(onNavigationIconListener: View.OnClickListener? = null,
                        vararg menuActionItem: MenuActions.MenuActionItem)
}