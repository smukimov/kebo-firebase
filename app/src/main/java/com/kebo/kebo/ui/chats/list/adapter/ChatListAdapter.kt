package com.kebo.kebo.ui.chats.list.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.firebase.ui.database.FirebaseIndexArray
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatInfoType
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.ui.global.RecyclerAdapterChangeListener
import com.kebo.kebo.ui.global.viewholder.BaseViewHolder

abstract class ChatListAdapter<T>
    (private val chatGroups: FirebaseIndexArray<T>) : RecyclerAdapterChangeListener() {

    override fun startListening() {
        if (!chatGroups.isListening(this)) {
            chatGroups.addChangeEventListener(this)
        }
    }

    override fun cleanup() {
        notifyItemRangeRemoved(0, itemCount)
        chatGroups.removeChangeEventListener(this)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item: T = getItem(position)
        @Suppress("UNCHECKED_CAST")
        val chatHolder = holder as BaseViewHolder<T>
        chatHolder.bind(item)
        chatHolder.onItemClick(getOnItemClickListener(item))
    }

    override fun getItemCount(): Int = if (chatGroups.isListening(this)) chatGroups.size else 0

    fun getItem(position: Int): T = chatGroups.getObject(position)

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return when (item) {
            is ChatPrivateInfo -> ChatInfoType.PRIVATE.ordinal
            is ChatGroupInfo -> ChatInfoType.GROUP.ordinal
            else -> throw Exception("Chat view type is not supported yet!")
        }
    }

    abstract fun getOnItemClickListener(item: T): View.OnClickListener
}