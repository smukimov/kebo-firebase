package com.kebo.kebo.ui.global.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import io.reactivex.disposables.Disposable

abstract class BaseViewHolder<in T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var subscription: Disposable? = null
    abstract fun bind(item: T)
    abstract fun onItemClick(listener: View.OnClickListener)
}
