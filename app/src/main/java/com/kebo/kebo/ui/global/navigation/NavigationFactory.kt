package com.kebo.kebo.ui.global.navigation

interface NavigationFactory {

    fun navigation(id: Int): Navigation

}