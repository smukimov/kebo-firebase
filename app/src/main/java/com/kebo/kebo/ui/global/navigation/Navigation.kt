package com.kebo.kebo.ui.global.navigation

import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.kebo.kebo.R
import com.kebo.kebo.extension.inflate
import com.kebo.kebo.ui.global.navigation.constants.NavigationIconType
import com.kebo.kebo.ui.global.navigation.menu.MenuActions

abstract class Navigation (private val layoutRes: Int) : INavigation {

    internal var toolbarId = R.id.toolbar
    internal var toolbarNavigationIcon: NavigationIconType = NavigationIconType.NO_NAV_ICON
    internal var toolbarNavigationIconListener: View.OnClickListener? = null
    internal var toolbarTitleRes: Int = 0
    internal var toolbarTitle: CharSequence? = null
    internal var toolbarSubtitleRes: Int = 0
    internal var toolbarSubtitle: CharSequence? = null
    internal var toolbarLogoRes: Int = 0
    internal var toolbarLogo: Drawable? = null
    internal var showProgress: Boolean = false

    internal var menuRes: MutableList<Int> = ArrayList()
    var menuActions = MenuActions.Builder()

    fun toolbarNavigationIcon(toolbarNavigationIcon: NavigationIconType) {
        val navIcon = navigationIcons.find { it.type == toolbarNavigationIcon }
        if (navIcon == null && toolbarNavigationIcon != NavigationIconType.NO_NAV_ICON) {

            throw IllegalArgumentException(
                    "There is no navigation icon for type: " + toolbarNavigationIcon)
        }
        this.toolbarNavigationIcon = navIcon!!.type
    }

    fun menuRes(menuRes: Int, menuBuilder: MenuActions.Builder) {
        this.menuRes.add(menuRes)
        this.menuActions.append(menuBuilder)
    }

    val navigationIcons: MutableList<NavigationIcon> = mutableListOf()

    init {
        navigationIcon(NavigationIconType.BACK, R.drawable.ic_arrow_back)
        navigationIcon(NavigationIconType.MENU, R.drawable.ic_menu)
    }

    fun navigationIcon(type: NavigationIconType, @DrawableRes drawableRes: Int) =
            navigationIcon(NavigationIcon(type, drawableRes))

    fun navigationIcon(navigationIcon: NavigationIcon) {
        this.navigationIcons.add(navigationIcon)
    }

    fun produceLayout(inflater: LayoutInflater, container: ViewGroup?): View {
        val parent = LinearLayout(inflater.context)
        parent.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT)
        parent.orientation = LinearLayout.VERTICAL

        val child = container!!.inflate(layoutRes, false)
        val childParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT)

        inflater.inflate(R.layout.toolbar, parent)
        parent.addView(child, childParams)
        return parent
    }
}