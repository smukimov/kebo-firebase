package com.kebo.kebo.ui.chats.list

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.firebase.ui.database.FirebaseIndexArray
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatInfoType
import com.kebo.kebo.presentation.chats.list.favorites.ChatFavoriteListContract
import com.kebo.kebo.presentation.chats.list.favorites.ChatFavoriteListPresenter
import com.kebo.kebo.ui.chats.list.adapter.ChatListAdapter
import com.kebo.kebo.ui.global.BaseFragment
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import kotlinx.android.synthetic.main.fragment_chat_list.view.*
import mu.KLogging
import javax.inject.Inject

@ActivityScoped
class ChatFavoriteListFragment @Inject constructor() : BaseFragment(),
        ChatFavoriteListContract.View {
    @Inject lateinit var presenterProviderChat: dagger.Lazy<ChatFavoriteListPresenter>
    @InjectPresenter lateinit var presenterChat: ChatFavoriteListPresenter
    @ProvidePresenter
    fun provideFavoriteListPresenter(): ChatFavoriteListPresenter = presenterProviderChat.get()
    @Inject
    lateinit var viewHolderFactory: Map<ChatInfoType, @JvmSuppressWildcards ViewHolderFactory>

    private val adapter: FavoriteListAdapter by lazy {
        FavoriteListAdapter(presenterChat.observeFavoriteList())
    }

    override fun onStart() {
        super.onStart()

        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()

        adapter.cleanup()
    }

    override val screenType: ScreenType
        get() = ScreenType.CHATS_FAVORITES

    override val layoutRes: Int
        get() = R.layout.fragment_chat_list

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        buildNavigation(View.OnClickListener { presenterChat.onBackPressed() })
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, parent: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = super.onCreateView(inflater, parent, savedInstanceState)!!
        root.companiesRV.layoutManager = LinearLayoutManager(context)
        root.companiesRV.adapter = adapter
        return root
    }

    override fun onBackPressed() = presenterChat.onBackPressed()

    inner class FavoriteListAdapter(chatGroupInfoList: FirebaseIndexArray<ChatGroupInfo>)
        : ChatListAdapter<ChatGroupInfo>(chatGroupInfoList) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
                viewHolderFactory[ChatInfoType.values()[viewType]]!!.createViewHolder(parent)

        override fun getOnItemClickListener(item: ChatGroupInfo) =
                View.OnClickListener {
                    presenterChat.onChatClicked(item.id())
                }
    }

    companion object : KLogging()
}