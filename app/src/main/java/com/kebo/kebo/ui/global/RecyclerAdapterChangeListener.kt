package com.kebo.kebo.ui.global

import android.support.v7.widget.RecyclerView
import com.firebase.ui.database.ChangeEventListener
import com.firebase.ui.database.ChangeEventListener.EventType.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import mu.KLogging

abstract class RecyclerAdapterChangeListener
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ChangeEventListener {

    override fun onCancelled(error: DatabaseError) { logger.error { error.toException() } }

    override fun onDataChanged() { }

    override fun onChildChanged(type: ChangeEventListener.EventType?,
                                snapshot: DataSnapshot?, index: Int, oldIndex: Int) {
        when (type) {
            ADDED -> notifyItemInserted(index)
            CHANGED -> notifyItemChanged(index)
            REMOVED -> notifyItemRemoved(index)
            MOVED -> notifyItemMoved(oldIndex, index)
            else -> throw IllegalStateException("Incomplete case statement")
        }
    }

    abstract fun startListening()
    abstract fun cleanup()

    companion object: KLogging()
}