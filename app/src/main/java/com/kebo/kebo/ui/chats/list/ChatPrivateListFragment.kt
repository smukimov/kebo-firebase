package com.kebo.kebo.ui.chats.list

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseIndexArray
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.models.interactors.ChatInfoType
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.presentation.chats.list.privates.ChatPrivateListContract
import com.kebo.kebo.presentation.chats.list.privates.ChatPrivateListPresenter
import com.kebo.kebo.ui.chats.list.adapter.ChatListAdapter
import com.kebo.kebo.ui.global.BaseFragment
import com.kebo.kebo.ui.global.viewholder.BaseViewHolder
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_chat_list.view.*
import kotlinx.android.synthetic.main.item_chat.view.*
import javax.inject.Inject

@ActivityScoped
class ChatPrivateListFragment @Inject constructor() : BaseFragment(), ChatPrivateListContract.View {
    @Inject lateinit var presenterChatProvider: dagger.Lazy<ChatPrivateListPresenter>
    @InjectPresenter lateinit var presenterChat: ChatPrivateListPresenter
    @ProvidePresenter
    fun providePrivateListPresenter(): ChatPrivateListPresenter = presenterChatProvider.get()

    @Inject
    lateinit var viewHolderFactory: Map<ChatInfoType, @JvmSuppressWildcards ViewHolderFactory>

    private val adapter: PrivateListAdapter by lazy {
        PrivateListAdapter(presenterChat.observePrivateList())
    }

    override fun onStart() {
        super.onStart()

        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()

        adapter.cleanup()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        buildNavigation(View.OnClickListener { presenterChat.onBackPressed() })
        super.onViewCreated(view, savedInstanceState)
    }

    override val screenType: ScreenType
        get() = ScreenType.CHATS_PRIVATE

    override val layoutRes: Int
        get() = R.layout.fragment_chat_list

    override fun onBackPressed() = presenterChat.onBackPressed()

    override fun onCreateView(inflater: LayoutInflater?, parent: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = super.onCreateView(inflater, parent, savedInstanceState)!!
        root.companiesRV.layoutManager = LinearLayoutManager(context)
        root.companiesRV.adapter = adapter
        return root
    }

    inner class PrivateListAdapter(chatGroups: FirebaseIndexArray<ChatPrivateInfo>)
        : ChatListAdapter<ChatPrivateInfo>(chatGroups) {

        val disposables = CompositeDisposable()

        override fun getOnItemClickListener(item: ChatPrivateInfo) =
                View.OnClickListener {
                    presenterChat.onChatClicked(item.chatId())
                }

        override fun onDetachedFromRecyclerView(recyclerView: RecyclerView?) {
            super.onDetachedFromRecyclerView(recyclerView)
            disposables.clear()
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val item: ChatPrivateInfo = getItem(position)
            val chatHolder = holder as BaseViewHolder<ChatPrivateInfo>
            if (chatHolder.subscription != null && !chatHolder.subscription!!.isDisposed) {
                disposables.remove(chatHolder.subscription!!)
            }
            chatHolder.subscription = presenterChat
                    .observeUserProfile(item.getPeer(presenterChat.currentUserId()))
                    .subscribe {
                        chatHolder.bind(ChatPrivateInfo.create(it))
                        chatHolder.itemView.chatName.text = it.displayName

                        if (!it.photoURL.isNullOrEmpty()) {
                            Glide.with(chatHolder.itemView.context)
                                    .using(FirebaseImageLoader())
                                    .load(it.photoURLRef)
                                    .into(chatHolder.itemView.chatSplash)
                        } else {
                            chatHolder.itemView.chatSplash.setImageResource(R.drawable.ic_account_circle_black_36dp)
                        }
                    }
            chatHolder.subscription!!.addTo(disposables)
            chatHolder.onItemClick(getOnItemClickListener(item))
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
                viewHolderFactory[ChatInfoType.values()[viewType]]!!.createViewHolder(parent)
    }
}