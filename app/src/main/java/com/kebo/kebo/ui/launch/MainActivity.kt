package com.kebo.kebo.ui.launch

import android.content.Intent
import android.os.Bundle
import com.kebo.kebo.R

import kotlinx.android.synthetic.main.activity_main.*

import javax.inject.Inject
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kebo.kebo.ui.global.BaseActivity
import com.kebo.kebo.ScreenType
import com.kebo.kebo.presentation.drawer.NavigationDrawerContract
import com.kebo.kebo.presentation.global.GlobalMenuController
import com.kebo.kebo.presentation.launch.LaunchContract
import com.kebo.kebo.presentation.launch.LaunchPresenter
import com.kebo.kebo.ui.auth.AuthActivity
import com.kebo.kebo.ui.chats.create.ChatCreateFragment
import com.kebo.kebo.ui.chats.show.ChatFragment
import com.kebo.kebo.ui.chats.list.ChatIntroListFragment
import com.kebo.kebo.ui.chats.show.ChatDetailsFragment
import com.kebo.kebo.ui.chats.edit.ChatEditFragment
import com.kebo.kebo.ui.drawer.NavigationDrawerFragment
import com.kebo.kebo.ui.chats.list.ChatFavoriteListFragment
import com.kebo.kebo.ui.chats.list.ChatPrivateListFragment
import com.kebo.kebo.ui.global.BaseFragment
import com.kebo.kebo.ui.settings.SettingsActivity
import io.reactivex.disposables.Disposable
import mu.KLogging
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class MainActivity : BaseActivity(), LaunchContract.View {
    @Inject lateinit var navigationHolder: NavigatorHolder
    @Inject lateinit var chatIntroIntroListFragmentProvider: dagger.Lazy<ChatIntroListFragment>
    @Inject lateinit var chatPrivateListFragmentProvider: dagger.Lazy<ChatPrivateListFragment>
    @Inject lateinit var chatFavoriteListFragmentProvider: dagger.Lazy<ChatFavoriteListFragment>
    @Inject lateinit var menuController: GlobalMenuController
    @Inject lateinit var presenterProvider: dagger.Lazy<LaunchPresenter>
    @InjectPresenter lateinit var presenter: LaunchPresenter
    @ProvidePresenter fun provideLaunchPresenter(): LaunchPresenter = presenterProvider.get()

    private var menuStateDisposable: Disposable? = null

    override fun onResumeFragments() {
        super.onResumeFragments()
        menuStateDisposable = menuController.state.subscribe { openNavDrawer(it) }
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        menuStateDisposable?.dispose()
        navigationHolder.removeNavigator()
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        drawerLayout.setStatusBarBackground(R.color.colorPrimary)
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            openNavDrawer(false)
        } else {
            val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)
            if (fragment is BaseFragment) {
                fragment.onBackPressed()
            } else {
                presenter.onBackPressed()
            }
        }
    }

    private fun enableNavDrawer(enable: Boolean) {
        drawerLayout.setDrawerLockMode(
                if (enable) DrawerLayout.LOCK_MODE_UNLOCKED
                else DrawerLayout.LOCK_MODE_LOCKED_CLOSED,
                GravityCompat.START
        )
    }


    fun openNavDrawer(open: Boolean) {
        drawerLayout.postDelayed({
            if (open) drawerLayout.openDrawer(GravityCompat.START)
            else drawerLayout.closeDrawer(GravityCompat.START)
        }, 150)
    }

    private val navigator = object : SupportAppNavigator(this, R.id.mainContainer) {

        override fun applyCommand(command: Command?) {
            super.applyCommand(command)
            updateNavDrawer()
        }

        override fun createActivityIntent(screenKey: String, data: Any?) =
                when (ScreenType.valueOf(screenKey)) {
                    ScreenType.AUTH -> Intent(this@MainActivity,
                            AuthActivity::class.java)
                    ScreenType.SETTINGS -> Intent(this@MainActivity,
                            SettingsActivity::class.java)
                    else -> null
                }

        override fun createFragment(screenKey: String, data: Any?): Fragment? =
                when (ScreenType.valueOf(screenKey)) {
                    ScreenType.CHATS_PRIVATE -> chatPrivateListFragmentProvider.get()
                    ScreenType.CHATS_INTRO -> chatIntroIntroListFragmentProvider.get()
                    ScreenType.CHATS_FAVORITES -> chatFavoriteListFragmentProvider.get()
                    ScreenType.CHAT_CREATE -> ChatCreateFragment()
                    ScreenType.CHAT_GROUP -> ChatFragment.createInstance(data as String)
                    ScreenType.CHAT_PRIVATE -> ChatFragment
                            .createInstance(data as String, true)
                    ScreenType.CHAT_GROUP_DETAILS -> ChatDetailsFragment
                            .createInstance(data as String)
                    ScreenType.CHAT_PRIVATE_DETAILS -> ChatDetailsFragment
                            .createInstance(data as String, true)
                    ScreenType.CHAT_EDIT -> ChatEditFragment.createInstance(data as String)
                    else -> null
                }
    }

    private fun updateNavDrawer() {
        supportFragmentManager.executePendingTransactions()

        val drawerFragment = supportFragmentManager.findFragmentById(R.id.navigationDrawer)
                as NavigationDrawerFragment
        supportFragmentManager.findFragmentById(R.id.mainContainer)?.let {
            when (it) {
                is ChatIntroListFragment -> drawerFragment
                        .onScreenChanged(NavigationDrawerContract.View.MenuItem.CHATS_INTRO)
                is ChatFavoriteListFragment -> drawerFragment
                        .onScreenChanged(NavigationDrawerContract.View.MenuItem.FAVORITES)
            }
            enableNavDrawer(isNavDrawerAvailableForFragment(it))
        }
    }

    private fun isNavDrawerAvailableForFragment(currentFragment: Fragment) = when (currentFragment) {
        is ChatIntroListFragment -> true
        is ChatFavoriteListFragment -> true
        else -> false
    }

    companion object : KLogging()
}
