package com.kebo.kebo.ui.chats.list

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*

import com.kebo.kebo.R
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.presentation.chats.list.intros.ChatIntroListContract
import com.kebo.kebo.presentation.chats.list.intros.ChatIntroListPresenter

import kotlinx.android.synthetic.main.fragment_chat_list.*
import mu.KLogging
import javax.inject.Inject
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.widget.AutoCompleteTextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.firebase.ui.database.FirebaseIndexArray
import com.kebo.kebo.ScreenType
import com.kebo.kebo.ui.global.BaseFragment
import io.reactivex.disposables.CompositeDisposable
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import com.jakewharton.rxbinding2.widget.RxAutoCompleteTextView
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.extension.visible
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatInfoType
import com.kebo.kebo.ui.chats.list.adapter.ChatListAdapter
import com.kebo.kebo.ui.chats.list.adapter.ChatLookupAdapter
import com.kebo.kebo.ui.global.navigation.menu.MenuAction
import com.kebo.kebo.ui.global.navigation.menu.MenuActions
import java.util.concurrent.TimeUnit

@ActivityScoped
class ChatIntroListFragment @Inject constructor() : BaseFragment(), ChatIntroListContract.View {
    @Inject lateinit var presenterProviderIntro: dagger.Lazy<ChatIntroListPresenter>
    @InjectPresenter lateinit var presenterIntro: ChatIntroListPresenter
    @ProvidePresenter
    fun provideCompaniesListPresenter(): ChatIntroListPresenter = presenterProviderIntro.get()

    @Inject
    lateinit var viewHolderFactory: Map<ChatInfoType, @JvmSuppressWildcards ViewHolderFactory>
    private val adapter: ChatIntroListAdapter by lazy {
        ChatIntroListAdapter(presenterIntro.observeCompanies())
    }
    private var disposables: CompositeDisposable = CompositeDisposable()

    override val layoutRes: Int
        get() = R.layout.fragment_chat_list

    override val screenType: ScreenType
        get() = ScreenType.CHATS_INTRO

    override fun onDetach() {
        super.onDetach()
        disposables.clear()
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.cleanup()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val createChatAction =
                MenuActions.MenuActionItem(R.id.menu_chat_create, object : MenuAction {
                    override fun execute(menuItem: MenuItem) {
                        presenterIntro.createChat()
                    }
                })
        buildNavigation(View.OnClickListener { presenterIntro.onMenuPressed() }, createChatAction)
        super.onViewCreated(view, savedInstanceState)

        getActionView<SearchView>(R.id.search).apply {
            visibility = View.VISIBLE
            this.setOnClickListener {
                this.setIconifiedByDefault(false)
            }
            val searchTextId = android.support.v7.appcompat.R.id.search_src_text
            val adapter = ChatLookupAdapter()

            RxAutoCompleteTextView.itemClickEvents(
                    findViewById<AutoCompleteTextView>(searchTextId).apply {
                        setAdapter(adapter)
                    }).subscribe {
                presenterIntro.onHitClicked(it.view().getItemAtPosition(it.position()))
            }.addTo(disposables)

            RxSearchView.queryTextChanges(this)
                    .debounce(150, TimeUnit.MILLISECONDS)
                    .switchMapSingle { presenterIntro.queryChat(it) }
                    .subscribe { adapter.replaceData(it) }.addTo(disposables)
        }
        fragmentTitleTV.text = getString(R.string.title_companies)
        companiesRV.layoutManager = LinearLayoutManager(context)
        companiesRV.adapter = adapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenterIntro.result(requestCode, resultCode)
    }

    override fun showNoCompanies() {
        noCompaniesLL.visible(true)
    }

    override fun onBackPressed() {
        presenterIntro.onBackPressed()
    }

    inner class ChatIntroListAdapter(chatGroups: FirebaseIndexArray<ChatGroupInfo>)
        : ChatListAdapter<ChatGroupInfo>(chatGroups) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
                viewHolderFactory[ChatInfoType.values()[viewType]]!!.createViewHolder(parent)

        override fun getOnItemClickListener(item: ChatGroupInfo) =
                View.OnClickListener {
                    presenterIntro.onChatClicked(item.chatGroup.id!!)
                }
    }

    companion object : KLogging()
}