package com.kebo.kebo.ui.global.viewholder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

interface ViewHolderFactory {

    fun createViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder

}
