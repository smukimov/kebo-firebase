package com.kebo.kebo.ui.global

import android.content.Intent
import android.os.Bundle

import com.google.firebase.auth.FirebaseAuth
import com.crashlytics.android.Crashlytics
import com.kebo.kebo.BuildConfig
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.ui.auth.AuthActivity
import io.fabric.sdk.android.Fabric
import ru.terrakok.cicerone.Router
import javax.inject.Inject

abstract class BaseActivity : BaseActivityMvp(), FirebaseAuth.AuthStateListener {
    @Inject lateinit var auth: FirebaseAuth
    @Inject lateinit var router: Router

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (BuildConfig.ENABLE_CRASHLYTICS) {
            Fabric.with(this, Crashlytics())
        }

        if (!isSignedIn) {
            redirectToSignIn()
        }
    }

    override fun onAuthStateChanged(firebaseAuth: FirebaseAuth) {
        if (firebaseAuth.currentUser == null) {
            redirectToSignIn()
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        auth.removeAuthStateListener(this)
    }

    public override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(this)

        if (!isSignedIn) {
            redirectToSignIn()
        }
    }

    public override fun onResume() {
        super.onResume()

        if (!isSignedIn) {
            redirectToSignIn()
        }
    }

    private val isSignedIn: Boolean
        get() = auth.currentUser != null

    private fun redirectToSignIn() {
        router.navigateTo(ScreenType.AUTH)
        finish()
    }
}
