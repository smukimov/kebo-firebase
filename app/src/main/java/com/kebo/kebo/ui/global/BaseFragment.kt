package com.kebo.kebo.ui.global

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kebo.kebo.ScreenType
import com.kebo.kebo.ui.global.navigation.INavigation
import com.kebo.kebo.ui.global.navigation.Navigation
import com.kebo.kebo.ui.global.navigation.NavigationFactory
import com.kebo.kebo.ui.global.navigation.constants.NavigationIconType
import com.kebo.kebo.ui.global.navigation.menu.MenuActions
import javax.inject.Inject

abstract class BaseFragment : BaseFragmentMvp(), INavigation {
    @Inject lateinit var navigationFactories: Map<ScreenType, @JvmSuppressWildcards NavigationFactory>

    open fun onBackPressed() {}

    abstract val screenType: ScreenType
    abstract val layoutRes: Int

    var navigation: Navigation? = null
    private var toolbar: Toolbar? = null

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        toolbar = view!!.findViewById(navigation!!.toolbarId)

        if (toolbar != null) {
            prepareToolbar(toolbar!!)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, parent: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val screenKey = if (navigationFactories.containsKey(screenType)) screenType
                        else ScreenType.DUMMY
        navigation = navigationFactories[screenKey]!!.navigation(layoutRes)
        return navigation!!.produceLayout(inflater!!, parent)
    }

    fun setTitle(title: String) {
        toolbar!!.title = " $title"
    }

    fun setSubtitle(subtitle: String) {
        toolbar!!.subtitle = " $subtitle"
    }

    fun setLogo(logo: Drawable) {
        toolbar!!.logo = logo
    }

    fun getLogoView(): View = toolbar!!.getChildAt(5) ?: toolbar!!.getChildAt(4)!!

    override fun buildNavigation(onNavigationIconListener: View.OnClickListener?,
                                 vararg menuActionItem: MenuActions.MenuActionItem) {
        navigation!!.buildNavigation(onNavigationIconListener, *menuActionItem)
    }

    fun <T : View?> getActionView(id: Int): T = toolbar!!.findViewById<T>(id)

    private fun prepareToolbar(toolbar: Toolbar) {
        with(navigation!!) {
            if (toolbarTitleRes != 0) {
                toolbar.setTitle(toolbarTitleRes)
            } else {
                toolbar.title = toolbarTitle
            }
            if (toolbarSubtitleRes != 0) {
                toolbar.setSubtitle(toolbarSubtitleRes)
            } else {
                toolbar.subtitle = toolbarSubtitle
            }
            if (toolbarLogoRes != 0) {
                toolbar.setLogo(toolbarLogoRes)
            } else {
                toolbar.logo = toolbarLogo
            }
            if (toolbarNavigationIcon == NavigationIconType.NO_NAV_ICON) {
                toolbar.navigationIcon = null
                toolbar.setNavigationOnClickListener(null)
            } else {
                val navIcon = navigationIcons.find { it.type == toolbarNavigationIcon }!!
                toolbar.navigationIcon = navIcon.iconDrawable(toolbar.context)
                toolbar.setNavigationOnClickListener(
                        toolbarNavigationIconListener ?: View.OnClickListener { })
            }

            toolbar.menu?.clear()

            if (menuRes.isNotEmpty()) {
                val actions = menuActions.build()

                for (menuRes in menuRes) {
                    toolbar.inflateMenu(menuRes)
                }

                toolbar.setOnMenuItemClickListener {
                    actions.onMenuItemClick(it)
                }
            }
        }
    }
}