package com.kebo.kebo.ui.chats.edit

import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.presentation.chats.edit.ChatEditContract
import com.kebo.kebo.presentation.chats.edit.ChatEditPresenter
import com.kebo.kebo.ui.global.BaseFragment
import com.kebo.kebo.ui.global.navigation.menu.MenuAction
import com.kebo.kebo.ui.global.navigation.menu.MenuActions
import kotlinx.android.synthetic.main.fragment_chat_edit.*
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ChatEditFragment @Inject constructor() : BaseFragment(), ChatEditContract.View {
    @Inject lateinit var router: Router
    @Inject lateinit var presenterProvider: dagger.Lazy<ChatEditPresenter>
    @InjectPresenter lateinit var presenter: ChatEditPresenter
    @ProvidePresenter
    fun provideChatEditPresenter(): ChatEditPresenter = presenterProvider.get()

    var chatId = lazy {
        arguments.getString(ChatEditFragment.ARG_CHAT_ID)
    }

    override val screenType: ScreenType
        get() = ScreenType.CHAT_EDIT

    override val layoutRes: Int
        get() = R.layout.fragment_chat_edit

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val editChatDoneAction =
                MenuActions.MenuActionItem(R.id.menu_chat_edit_done, object : MenuAction {
                    override fun execute(menuItem: MenuItem) {
                        if (validateForm()) {
                            val chatName = chatNameET.text.toString()
                            presenter
                                    .updateChatName(chatId.value, chatName)
                                    .subscribe {
                                        router.exit()
                                    }
                        }
                    }
                })

        buildNavigation(View.OnClickListener {
            router.exit()
        }, editChatDoneAction)
        super.onViewCreated(view, savedInstanceState)

        presenter.initChatToEdit(chatId.value)
    }

    private fun validateForm(): Boolean {
        var result = true
        chatNameET.error = null
        val chatName = chatNameET.text.toString()

        if (TextUtils.isEmpty(chatName)) {
            chatNameET.error = getString(R.string.error_field_required)
            result = false
        }

        return result
    }

    override fun setChatInfoToEdit(chatGroupInfo: ChatGroupInfo) {
        chatNameET.setText(chatGroupInfo.name)
    }

    companion object IntentOptions {
        private const val ARG_CHAT_ID = "chat_id"

        fun createInstance(chatId: String) = ChatEditFragment().apply {
            arguments = Bundle().also { it.putString(ARG_CHAT_ID, chatId) }
        }
    }
}