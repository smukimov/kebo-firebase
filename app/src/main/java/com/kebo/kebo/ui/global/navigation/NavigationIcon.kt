package com.kebo.kebo.ui.global.navigation

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v4.graphics.drawable.DrawableCompat.setTint
import android.support.graphics.drawable.VectorDrawableCompat.create
import android.support.v4.content.ContextCompat
import com.kebo.kebo.ui.global.navigation.constants.NavigationIconType

class NavigationIcon constructor(val type: NavigationIconType,
                                 @DrawableRes private val drawableRes: Int) {

    fun iconDrawable(context: Context): Drawable {
        val navIcon =
                create(context.resources, drawableRes, context.theme) as VectorDrawableCompat
        setTint(navIcon, ContextCompat.getColor(context, android.R.color.white))
        return navIcon
    }

}
