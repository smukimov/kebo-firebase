package com.kebo.kebo.ui.chats.show

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputFilter
import com.google.android.gms.appinvite.AppInviteInvitation
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.kebo.kebo.R
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.presentation.chats.show.ChatContract
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_chat.view.*
import mu.KLogging
import javax.inject.Inject
import android.view.*
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.kebo.kebo.ScreenType
import com.kebo.kebo.ui.global.BaseFragment
import com.kebo.kebo.ui.global.navigation.menu.MenuAction
import com.kebo.kebo.ui.global.navigation.menu.MenuActions
import com.kebo.kebo.extension.visible
import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.firebase.ui.database.FirebaseArray
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.Message
import com.kebo.kebo.entities.MessageType
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.ui.chats.show.adapter.MessageListAdapterBase
import com.kebo.kebo.models.interactors.MessageInfo
import com.kebo.kebo.presentation.chats.show.ChatGroupPresenter
import com.kebo.kebo.presentation.chats.show.ChatPresenter
import com.kebo.kebo.presentation.chats.show.ChatPrivatePresenter
import com.kebo.kebo.ui.global.viewholder.BaseViewHolder
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import kotlinx.android.synthetic.main.activity_settings.view.*
import ru.terrakok.cicerone.Router

@ActivityScoped
class ChatFragment @Inject constructor() : BaseFragment(), ChatContract.View {
    @Inject lateinit var groupPresenterProvider: dagger.Lazy<ChatGroupPresenter>
    @Inject lateinit var privatePresenterProvider: dagger.Lazy<ChatPrivatePresenter>
    @Inject lateinit var router: Router
    @InjectPresenter lateinit var presenter: ChatPresenter
    @ProvidePresenter
    fun provideChatPresenter(): ChatPresenter =
            if (isPrivate.value) privatePresenterProvider.get() else groupPresenterProvider.get()
    @Inject
    lateinit var viewHolderFactory: Map<MessageType, @JvmSuppressWildcards ViewHolderFactory>

    private val adapterBase: MessageListAdapterBase by lazy {
        MessagesListAdapter(presenter.messagePublisher(chatId.value))
    }
    private var linearLayoutManager: LinearLayoutManager? = null
    private val disposables = CompositeDisposable()

    val chatId: Lazy<String> = lazy { arguments.getString(ARG_CHAT_ID) }
    private val isPrivate: Lazy<Boolean> = lazy { arguments.getBoolean(ARG_CHAT_PRIVATE) }

    override val layoutRes: Int
        get() = R.layout.fragment_chat

    override val screenType: ScreenType
        get() = if (isPrivate.value) ScreenType.CHAT_PRIVATE else ScreenType.CHAT_GROUP

    companion object IntentOptions : KLogging() {
        val REQUEST_IMAGE = 2
        private val REQUEST_INVITE = 1
        private const val ARG_CHAT_ID = "chat_id"
        private const val ARG_CHAT_PRIVATE = "chat_private"

        fun createInstance(chatId: String, private: Boolean = false) = ChatFragment().apply {
            arguments = Bundle().also {
                it.putString(ARG_CHAT_ID, chatId)
                it.putBoolean(ARG_CHAT_PRIVATE, private)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        adapterBase.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapterBase.cleanup()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val addToFavoriteAction = MenuActions.MenuActionItem(R.id.menu_add_to_favorite,
                object : MenuAction {
                    override fun execute(menuItem: MenuItem) {
                        presenter.addToFavorites(chatId.value)
                    }
                })
        buildNavigation(View.OnClickListener {
            presenter.onBackPressed()
        }, addToFavoriteAction)

        super.onViewCreated(view, savedInstanceState)

        disposables.addAll(RxTextView.textChanges(messageET).subscribe({
            presenter.onMessageEditChanged(it)
        }), RxView.clicks(attachmentIV).subscribe({
            presenter.openImagePickDialog()
        }), RxView.clicks(sendButton).subscribe({
            presenter.sendMessage(chatId.value, messageET.text.toString())
        }), RxView.clicks(joinButton).subscribe({
            presenter.addToFavorites(chatId.value)
        }))

        messageET.filters =
                arrayOf<InputFilter>(InputFilter.LengthFilter(presenter.messageLengthLimit))

        presenter.initToolbar(chatId.value)

        val drawable = ContextCompat.getDrawable(activity, R.drawable.catstile) as BitmapDrawable
        drawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)
        recyclerView.background = drawable
    }

    override fun onCreateView(inflater: LayoutInflater?, parent: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = super.onCreateView(inflater, parent, savedInstanceState)!!
        linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager!!.stackFromEnd = true

        root.recyclerView.layoutManager = linearLayoutManager!!
        root.recyclerView.adapter = adapterBase

        adapterBase.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                val lastVisiblePosition =
                        linearLayoutManager!!.findLastCompletelyVisibleItemPosition()

                if (lastVisiblePosition == -1 || positionStart >= itemCount - 1 &&
                        lastVisiblePosition == positionStart - 1) {
                    scrollToPosition(positionStart)
                }
            }
        })

        return root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK && intent != null) {
            presenter.sendImage(chatId.value, intent.data)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.chat_group_fragment_menu, menu)
    }

    override fun scrollToPosition(position: Int) {
        recyclerView.smoothScrollToPosition(position)
    }

    override fun openImagePickDialog() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    override fun sendInvitation() {
        val intent = AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setCallToActionText(getString(R.string.invitation_cta))
                .build()
        startActivityForResult(intent, REQUEST_INVITE)
    }

    override fun clearMessageEditText() {
        messageET.setText("")
    }

    override fun toggleSendButton(showOrHide: Boolean) {
        sendButton.visible(showOrHide)
    }

    override fun toggleAttachmentButton(showOrHide: Boolean) {
        attachmentIV.visible(showOrHide)
    }

    override fun setChatName(name: String) {
        setTitle(name)
    }

    override fun setMemberCount(memberCount: String) {
        setSubtitle(memberCount)
    }

    override fun setLogo(logoRef: StorageReference) {
        if (context != null && isAdded) {
            Glide.with(context)
                    .using(FirebaseImageLoader())
                    .load(logoRef)
                    .asBitmap()
                    .override(156, 156)
                    .centerCrop()
                    .into(object : SimpleTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap?,
                                                     glideAnimation: GlideAnimation<in Bitmap>?) {
                            RoundedBitmapDrawableFactory.create(resources, resource).run {
                                this.isCircular = true
                                setLogo(this)
                                setLogoClickListener()
                            }
                        }
                    })
        }
    }

    override fun setLogoDefault() {
        setLogo(ContextCompat.getDrawable(context, R.drawable.ic_account_circle_black_36dp))
        setLogoClickListener()
    }

    override fun chatJoined(isJoined: Boolean) {
        messageLL.visible(isJoined)
        joinLL.visible(!isJoined)
    }

    override fun onBackPressed() = presenter.onBackPressed()

    fun setLogoClickListener() {
        RxView.clicks(getLogoView()).subscribe {
            val screenType =
                    if (isPrivate.value) ScreenType.CHAT_PRIVATE_DETAILS
                    else ScreenType.CHAT_GROUP_DETAILS
            router.navigateTo(screenType, chatId.value)
        }.addTo(disposables)
    }

    inner class MessagesListAdapter(messageList: FirebaseArray<Message>)
        : MessageListAdapterBase(messageList) {
        private val disposables = CompositeDisposable()

        override fun onAvatarClick(messageInfo: MessageInfo) {
            presenter.onAvatarClicked(messageInfo)
        }

        override fun onDetachedFromRecyclerView(recyclerView: RecyclerView?) {
            super.onDetachedFromRecyclerView(recyclerView)
            disposables.clear()
        }

        override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
            super.onBindViewHolder(viewHolder, position)
            val item: MessageInfo = getItem(position)
            val messageHolder = viewHolder as BaseViewHolder<MessageInfo>

            if (viewHolder.subscription != null && !viewHolder.subscription!!.isDisposed) {
                disposables.delete(viewHolder.subscription!!)
            }

            viewHolder.subscription = presenter.observeUserProfile(item.userId()).subscribe {
                messageHolder.itemView.nickTV.text = it.displayName
                if (!it.photoURL.isNullOrEmpty()) {
                    Glide.with(messageHolder.itemView.context)
                            .using(FirebaseImageLoader())
                            .load(it.photoURLRef)
                            .into(messageHolder.itemView.avatarIV)
                } else {
                    messageHolder.itemView.avatarIV.setImageResource(R.drawable.ic_account_circle_black_36dp)
                }
            }

            viewHolder.subscription!!.addTo(disposables)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
                viewHolderFactory[MessageType.values()[viewType]]!!.createViewHolder(parent)
    }
}