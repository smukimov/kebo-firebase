package com.kebo.kebo.ui.chats.list.viewholder

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.auto.factory.AutoFactory
import com.kebo.kebo.R
import com.kebo.kebo.extension.inflate
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.ui.global.viewholder.BaseViewHolder
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import kotlinx.android.synthetic.main.item_chat.view.*
import mu.KLogging

@AutoFactory(implementing = [(ViewHolderFactory::class)])
class ChatViewHolderGroup(parent: ViewGroup)
    : BaseViewHolder<ChatGroupInfo>(parent.inflate(R.layout.item_chat, false)) {

    override fun bind(item: ChatGroupInfo) {
        item.apply {
            itemView.chatName.text = chatGroup.name
            itemView.chatAddress.text = chatGroup.address
            itemView.chatFavoriteCount.text = chatGroup.favoriteCount.toString()

            if (hasLogo()) {
                Glide.with(itemView.context)
                        .using(FirebaseImageLoader())
                        .load(logoURLReference)
                        .into(itemView.chatSplash)
            } else {
                itemView.chatSplash.setImageResource(R.drawable.ic_account_circle_black_36dp)
            }
        }
    }

    override fun onItemClick(listener: View.OnClickListener) {
        itemView.setOnClickListener(listener)
    }

    companion object : KLogging()
}
