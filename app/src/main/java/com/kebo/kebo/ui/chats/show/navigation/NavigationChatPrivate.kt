package com.kebo.kebo.ui.chats.show.navigation

import android.view.View
import com.google.auto.factory.AutoFactory
import com.kebo.kebo.R
import com.kebo.kebo.ui.global.navigation.Navigation
import com.kebo.kebo.ui.global.navigation.NavigationFactory
import com.kebo.kebo.ui.global.navigation.constants.NavigationIconType
import com.kebo.kebo.ui.global.navigation.menu.MenuActions

@AutoFactory(implementing = [(NavigationFactory::class)])
class NavigationChatPrivate(id: Int) : Navigation(id) {

    override fun buildNavigation(onNavigationIconListener: View.OnClickListener?,
                                 vararg menuActionItem: MenuActions.MenuActionItem) {
        toolbarNavigationIcon(NavigationIconType.BACK)
        toolbarNavigationIconListener = onNavigationIconListener

        menuRes(R.menu.chat_private_fragment_menu, MenuActions.Builder(*menuActionItem))
    }
}