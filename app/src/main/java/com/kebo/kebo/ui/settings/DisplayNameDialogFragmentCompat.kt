package com.kebo.kebo.ui.settings

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.preference.PreferenceDialogFragmentCompat
import android.view.View
import android.widget.EditText

class DisplayNameDialogFragmentCompat : PreferenceDialogFragmentCompat() {
    private var mDisplayNameEditText: EditText? = null

    override fun onDialogClosed(positiveResult: Boolean) {
        if (positiveResult) {
            val displayName = mDisplayNameEditText!!.text.toString()

            val preference = preference
            if (preference is DisplayNamePreference) {

                if (preference.callChangeListener(displayName)) {
                    preference.displayName = displayName
                }
            }
        }
    }

    override fun onBindDialogView(view: View) {
        super.onBindDialogView(view)

        mDisplayNameEditText = view.findViewById(android.R.id.edit)

        if (mDisplayNameEditText == null) {
            throw IllegalStateException("Dialog view must contain a EditView with layoutRes 'edit'")
        }

        var displayName: String? = null
        val preference = preference
        if (preference is DisplayNamePreference) {
            displayName = preference.displayName
        }

        if (displayName != null) {
            mDisplayNameEditText!!.setText(displayName)
        }
    }

    companion object {

        fun newInstance(key: String): DialogFragment {
            val fragment = DisplayNameDialogFragmentCompat()
            val b = Bundle(1)
            b.putString(PreferenceDialogFragmentCompat.ARG_KEY, key)
            fragment.arguments = b

            return fragment
        }
    }
}
