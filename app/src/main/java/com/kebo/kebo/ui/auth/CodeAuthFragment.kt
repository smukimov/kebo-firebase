package com.kebo.kebo.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.jakewharton.rxbinding2.view.RxView
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.presentation.auth.AuthContract
import com.kebo.kebo.presentation.auth.CodeAuthPresenter
import com.kebo.kebo.ui.global.BaseFragmentMvp
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_code_auth.*
import mu.KLogging
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@ActivityScoped
class CodeAuthFragment @Inject constructor() : BaseFragmentMvp(), AuthContract.CodeAuthView {
    @Inject lateinit var presenterProvider: dagger.Lazy<CodeAuthPresenter>
    @Inject lateinit var router: Router
    @InjectPresenter lateinit var presenter: CodeAuthPresenter
    @ProvidePresenter
    fun provideSignInPresenter(): CodeAuthPresenter = presenterProvider.get()

    private val disposable = CompositeDisposable()

    companion object IntentOptions : KLogging() {
        private const val ARG_VERIFICATION_ID = "verificationId"

        fun createInstance(verificationId: String) = CodeAuthFragment().apply {
            arguments = Bundle().also {
                it.putString(ARG_VERIFICATION_ID, verificationId)
            }
        }
    }

    private val verificationId: Lazy<String> =
            lazy { arguments.getString(CodeAuthFragment.ARG_VERIFICATION_ID) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater!!.inflate(R.layout.fragment_code_auth, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(verifyCodeB).subscribe {
            val codeText = codeET.text.toString()
            if (codeText.isNotEmpty()) {
                presenter.verifyCode(verificationId.value, codeText)
            }

        }.addTo(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    override fun verifyCodeCompleted(authResult: Task<AuthResult>) {
        authResult.addOnCompleteListener(activity) { task ->
            if (task.isSuccessful) {
                router.navigateTo(ScreenType.CHATS_INTRO)
                activity.finish()
            }
        }
    }
}
