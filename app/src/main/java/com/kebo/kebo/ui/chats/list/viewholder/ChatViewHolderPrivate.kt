package com.kebo.kebo.ui.chats.list.viewholder

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.auto.factory.AutoFactory
import com.kebo.kebo.R
import com.kebo.kebo.extension.inflate
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.ui.global.viewholder.BaseViewHolder
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import kotlinx.android.synthetic.main.item_chat.view.*

@AutoFactory(implementing = [(ViewHolderFactory::class)])
class ChatViewHolderPrivate(parent: ViewGroup) :
        BaseViewHolder<ChatPrivateInfo>(parent.inflate(R.layout.item_chat, false)) {

    override fun bind(item: ChatPrivateInfo) {
    }

    override fun onItemClick(listener: View.OnClickListener) {
        itemView.setOnClickListener(listener)
    }
}