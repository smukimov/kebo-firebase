package com.kebo.kebo.ui.chats.show

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.firebase.storage.StorageReference
import com.jakewharton.rxbinding2.view.RxView
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.presentation.chats.details.ChatDetailsContract
import com.kebo.kebo.presentation.chats.details.ChatDetailsPresenter
import com.kebo.kebo.presentation.chats.details.ChatGroupDetailsPresenter
import com.kebo.kebo.presentation.chats.details.ChatPrivateDetailsPresenter
import com.kebo.kebo.ui.global.BaseFragment
import com.kebo.kebo.ui.global.navigation.menu.MenuAction
import com.kebo.kebo.ui.global.navigation.menu.MenuActions
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_chat_details.*
import mu.KLogging
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ChatDetailsFragment @Inject constructor() : BaseFragment(), ChatDetailsContract.View {
    @Inject lateinit var router: Router
    @Inject lateinit var chatGroupPresenterProvider: dagger.Lazy<ChatGroupDetailsPresenter>
    @Inject lateinit var chatPrivatePresenterProvider: dagger.Lazy<ChatPrivateDetailsPresenter>
    @InjectPresenter lateinit var presenter: ChatDetailsPresenter
    private val disposables = CompositeDisposable()

    @ProvidePresenter
    fun provideChatDetailsPresenter(): ChatDetailsPresenter {
        val isPrivate = arguments.getBoolean(ARG_CHAT_PRIVATE, false)
        val presenter =
                if (isPrivate) chatPrivatePresenterProvider.get()
                else chatGroupPresenterProvider.get()
        val chatId = arguments.getString(ARG_CHAT_ID)
        presenter.chatId = chatId
        return presenter
    }

    override val screenType: ScreenType
        get() =
            if (arguments.getBoolean(ARG_CHAT_PRIVATE, false)) ScreenType.CHAT_PRIVATE_DETAILS
            else ScreenType.CHAT_GROUP_DETAILS

    override val layoutRes: Int
        get() = R.layout.fragment_chat_details

    override fun showLogo(reference: StorageReference) {
        Glide.with(context)
                .using(FirebaseImageLoader())
                .load(reference)
                .into(chatLogoCIV)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == READ_REQUEST_CODE) {
            if (resultCode == MvpAppCompatActivity.RESULT_OK) {
                val fileUri = data?.data
                if (fileUri != null) {
                    presenter.setLogo(fileUri).subscribe {
                        router.showSystemMessage("Лого обновленно")
                    }
                } else {
                    logger.warn { "File URI is null" }
                }
            }
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        val editChatAction =
                MenuActions.MenuActionItem(R.id.menu_chat_edit, object : MenuAction {
                    override fun execute(menuItem: MenuItem) {
                        router.navigateTo(ScreenType.CHAT_EDIT, presenter.chatId)
                    }
                })
        buildNavigation(View.OnClickListener {
            router.exit()
        }, editChatAction)

        super.onViewCreated(view, savedInstanceState)

        presenter.initChat()

        RxView.clicks(chatLogoCIV).flatMap {
            RxPermissions(activity).request(Manifest.permission.READ_EXTERNAL_STORAGE)
        }.subscribe {
            if (it) {
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE)
                intent.type = "image/*"
                startActivityForResult(intent, READ_REQUEST_CODE)
            }
        }.addTo(disposables)
    }

    override fun setChatGroupInfo(chatGroupInfo: ChatGroupInfo) {
        chatNameTV.text = chatGroupInfo.name
        chatMemberCountTV.text = chatGroupInfo.favoriteCount
        chatAddressTV.text = chatGroupInfo.address

        if (chatGroupInfo.hasLogo()) {
            showLogo(chatGroupInfo.logoURLReference!!)
        }
    }

    override fun setChatPrivateInfo(chatPrivateInfo: ChatPrivateInfo) {
        presenter.getUserProfile(chatPrivateInfo.getPeer(presenter.getCurrentUserId())).subscribe {
            chatNameTV.text = it.displayName

            if (!it.photoURL.isNullOrEmpty()) {
                showLogo(it.photoURLRef)
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        disposables.clear()
    }

    companion object IntentOptions : KLogging() {
        private const val ARG_CHAT_ID = "chat_id"
        private const val ARG_CHAT_PRIVATE = "chat_private"
        private val READ_REQUEST_CODE = 42

        fun createInstance(chatId: String, isPrivate: Boolean = false) = ChatDetailsFragment().apply {
            arguments = Bundle().also {
                it.putBoolean(ARG_CHAT_PRIVATE, isPrivate)
                it.putString(ARG_CHAT_ID, chatId)
            }
        }
    }
}