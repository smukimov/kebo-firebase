package com.kebo.kebo.ui.chats.list.navigation

import android.view.View
import com.google.auto.factory.AutoFactory
import com.kebo.kebo.R
import com.kebo.kebo.ui.global.navigation.Navigation
import com.kebo.kebo.ui.global.navigation.NavigationFactory
import com.kebo.kebo.ui.global.navigation.constants.NavigationIconType
import com.kebo.kebo.ui.global.navigation.menu.MenuActions

@AutoFactory(implementing = [(NavigationFactory::class)])
class NavigationChatPrivateList(id: Int) : Navigation(id) {
    override fun buildNavigation(onNavigationIconListener: View.OnClickListener?,
                                 vararg menuActionItem: MenuActions.MenuActionItem) {
        toolbarNavigationIcon(NavigationIconType.BACK)
        toolbarNavigationIconListener = onNavigationIconListener
        toolbarTitleRes = R.string.title_inbox
    }
}