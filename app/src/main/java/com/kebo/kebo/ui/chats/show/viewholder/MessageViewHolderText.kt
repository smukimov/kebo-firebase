package com.kebo.kebo.ui.chats.show.viewholder

import android.view.View
import android.view.ViewGroup
import com.google.auto.factory.AutoFactory
import com.kebo.kebo.R
import com.kebo.kebo.extension.inflate
import com.kebo.kebo.models.interactors.MessageInfo
import com.kebo.kebo.ui.global.viewholder.BaseViewHolder
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import kotlinx.android.synthetic.main.item_message.view.*

@AutoFactory(implementing = [(ViewHolderFactory::class)])
class MessageViewHolderText(parent: ViewGroup)
    : BaseViewHolder<MessageInfo>(parent.inflate(R.layout.item_message, false)) {

    override fun bind(item: MessageInfo) {
        itemView.messageTV.text = item.text()
    }

    override fun onItemClick(listener: View.OnClickListener) {
    }
}