package com.kebo.kebo.ui.settings

interface PreferenceDialogResult {
    fun showToast(message: String)
}
