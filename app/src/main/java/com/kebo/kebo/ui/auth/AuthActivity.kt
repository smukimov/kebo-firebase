package com.kebo.kebo.ui.auth

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.crashlytics.android.Crashlytics
import com.google.firebase.auth.FirebaseAuth
import com.kebo.kebo.BuildConfig
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.ui.launch.MainActivity
import dagger.android.support.DaggerAppCompatActivity
import io.fabric.sdk.android.Fabric
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator

import javax.inject.Inject

class AuthActivity : DaggerAppCompatActivity() {
    @Inject lateinit var router: Router
    @Inject lateinit var navigationHolder: NavigatorHolder
    @Inject lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (BuildConfig.ENABLE_CRASHLYTICS) {
            Fabric.with(this, Crashlytics())
        }
        setContentView(R.layout.activity_auth)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigationHolder.removeNavigator()
        super.onPause()
    }

    override fun onStart() {
        super.onStart()

        when {
            auth.currentUser != null -> router.navigateTo(ScreenType.CHATS_INTRO)
            BuildConfig.DEBUG -> router.navigateTo(ScreenType.AUTH_EMAIL)
            else -> router.navigateTo(ScreenType.AUTH_PHONE)
        }
    }

    private val navigator = object : SupportAppNavigator(this, R.id.mainContainer) {
        override fun createActivityIntent(screenKey: String, data: Any?) =
                when (ScreenType.valueOf(screenKey)) {
                    ScreenType.CHATS_INTRO -> Intent(this@AuthActivity,
                            MainActivity::class.java)
                    else -> null
                }

        override fun createFragment(screenKey: String, data: Any?): Fragment? =
                when (ScreenType.valueOf(screenKey)) {
                    ScreenType.AUTH_PHONE -> PhoneAuthFragment()
                    ScreenType.AUTH_CODE -> CodeAuthFragment.createInstance(data as String)
                    ScreenType.AUTH_EMAIL -> EmailAuthFragment()
                    else -> null
                }
    }
}

