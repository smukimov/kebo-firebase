package com.kebo.kebo.ui.auth

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.jakewharton.rxbinding2.view.RxView
import com.kebo.kebo.R
import com.kebo.kebo.ScreenType
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.presentation.auth.AuthContract
import com.kebo.kebo.presentation.auth.PhoneAuthPresenter
import com.kebo.kebo.ui.global.BaseFragmentMvp
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_phone_auth.*
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ActivityScoped
class PhoneAuthFragment @Inject constructor() : BaseFragmentMvp(), AuthContract.PhoneAuthView {
    @Inject lateinit var presenterProvider: dagger.Lazy<PhoneAuthPresenter>
    @Inject lateinit var phoneAuthProvider: PhoneAuthProvider
    @Inject lateinit var router: Router
    @InjectPresenter lateinit var presenter: PhoneAuthPresenter
    @ProvidePresenter fun providePhoneAuthPresenter() = presenterProvider.get()

    private val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View =
            inflater!!.inflate(R.layout.fragment_phone_auth, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(sendSmsCodeB).subscribe {
            if (validateForm()) {
                val phoneText = phoneET.text.toString()
                verifyPhoneNumber(phoneText)
            }
        }.addTo(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    private fun validateForm(): Boolean {
        phoneET.error = null

        val phoneText = phoneET.text.toString()
        var result = true

        if (TextUtils.isEmpty(phoneText)) {
            phoneET.error = getString(R.string.error_field_required)
            result = false
        }

        if (phoneText.isNotEmpty() && phoneText[0] != '+') {
            phoneET.error = getString(R.string.error_field_phone_format)
            result = false
        }

        return result
    }

    override fun verifyPhoneNumber(phoneNumber: String) {
        val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                presenter.signInWithCredential(credential).addOnCompleteListener {
                    if (it.isSuccessful) {
                        router.navigateTo(ScreenType.CHATS_INTRO)
                        activity.finish()
                    } else {
                        router.showSystemMessage(it.exception?.message)
                    }
                }
            }

            override fun onVerificationFailed(e: FirebaseException?) {
                if (e is FirebaseAuthInvalidCredentialsException) {
                    router.showSystemMessage(getString(R.string.error_format))
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                }
            }
            override fun onCodeSent(verificationId: String?,
                                    token: PhoneAuthProvider.ForceResendingToken?) {
                super.onCodeSent(verificationId, token)
                router.navigateTo(ScreenType.AUTH_CODE, verificationId as Any)
                router.showSystemMessage(getString(R.string.wait_code))
            }
        }
        phoneAuthProvider
                .verifyPhoneNumber(phoneNumber,60, TimeUnit.SECONDS, activity, callbacks)
    }
}