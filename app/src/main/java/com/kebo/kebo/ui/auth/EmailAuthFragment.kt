package com.kebo.kebo.ui.auth

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.jakewharton.rxbinding2.view.RxView
import com.kebo.kebo.R
import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.presentation.auth.EmailAuthPresenter
import com.kebo.kebo.ui.global.BaseFragmentMvp
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_email_auth.*
import javax.inject.Inject

@ActivityScoped
class EmailAuthFragment @Inject constructor() : BaseFragmentMvp(), MvpView {
    @Inject lateinit var presenterProvider: dagger.Lazy<EmailAuthPresenter>
    @InjectPresenter lateinit var presenter: EmailAuthPresenter
    @ProvidePresenter fun provideEmailAuthPresenter() = presenterProvider.get()

    private val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View =
            inflater!!.inflate(R.layout.fragment_email_auth, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(signInB).subscribe {
            if (validateForm()) {
                val emailText = emailET.text.toString()
                val passwordText = passwordET.text.toString()
                presenter.signInWithEmailAndPassword(emailText, passwordText)
            }
        }.addTo(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    private fun validateForm(): Boolean {
        emailET.error = null

        val emailText = emailET.text.toString()
        var result = true

        if (TextUtils.isEmpty(emailText)) {
            emailET.error = getString(R.string.error_field_required)
            result = false
        }
        return result
    }
}