package com.kebo.kebo.entities

enum class MessageType {
    TEXT,
    IMAGE
}