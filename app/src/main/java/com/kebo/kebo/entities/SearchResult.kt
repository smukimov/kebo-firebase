package com.kebo.kebo.entities

class SearchResult {
    var hits: List<Hit>? = null
    var nbHits: Int? = null
    var page: Int? = null
    var nbPages: Int? = null
    var hitsPerPage: Int? = null
    var processingTimeMS: Long? = null
    var exhaustiveNbHits: Boolean? = null
    var query: String? = null
    var params: String? = null

    data class Hit(
        var name: String? = null,
        var logoURL: String? = null,
        var objectID: String? = null,
        var _highlightResult: Map<String, Any>? = null)
}