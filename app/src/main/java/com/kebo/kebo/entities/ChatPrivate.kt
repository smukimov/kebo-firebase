package com.kebo.kebo.entities

import java.util.HashMap

data class ChatPrivate(val userId: String? = null,
                       val chatId: String? = null,
                       val isPrivate: Boolean = true,
                       val favorites: MutableMap<String, Boolean> = HashMap())
