package com.kebo.kebo.entities

import com.google.firebase.database.IgnoreExtraProperties

import java.util.HashMap

@IgnoreExtraProperties
data class ChatGroup(val name: String = "Unknown",
                     val logoURL: String? = null,
                     val address: String = "",
                     var favoriteCount: Int = 0,
                     val id: String? = null,
                     val favorites: MutableMap<String, Boolean> = HashMap()) {

    fun toggleFavorite(uid: String): ChatGroup {
        if (favorites.containsKey(uid)) {
            favoriteCount -= 1
            favorites.remove(uid)
        } else {
            favoriteCount += 1
            favorites.put(uid, true)
        }
        return this
    }
}
