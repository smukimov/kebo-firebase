package com.kebo.kebo.entities

data class Message(val text: String? = null,
                   val userId: String? = null,
                   val imageUrl: String? = null,
                   val id: String? = null)
