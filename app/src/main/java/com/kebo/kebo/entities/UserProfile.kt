package com.kebo.kebo.entities

import com.google.firebase.database.Exclude
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.util.HashMap

data class UserProfile(
    val id: String? = null,
    val photoURL: String? = null,
    var chatCount: Int = 0,
    val chats: MutableMap<String, Boolean> = HashMap(),
    val displayName: String? = null) {

    val photoURLRef: StorageReference
        @Exclude
        get() = FirebaseStorage.getInstance("gs://kebo-93097-faces")
                .reference
                .child(photoURL!!)
}
