package com.kebo.kebo

import android.support.annotation.VisibleForTesting
import com.kebo.kebo.models.repository.ChatRepository
import com.kebo.kebo.dagger.AppComponent
import com.kebo.kebo.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject

class App : DaggerApplication() {
    @VisibleForTesting @Inject lateinit var chatRepository: ChatRepository

    override fun applicationInjector(): AndroidInjector<DaggerApplication> {
        val appComponent: AppComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}
