package com.kebo.kebo.models.repository

import android.net.Uri
import com.firebase.ui.database.SnapshotParser
import com.kebo.kebo.models.data.server.FirebaseApi
import com.kebo.kebo.entities.ChatGroup
import com.kebo.kebo.entities.ChatPrivate
import com.kebo.kebo.entities.Message
import com.kebo.kebo.models.data.server.AlgoliaApiProvider
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Singleton
import javax.inject.Inject

@Singleton
class ChatRepository @Inject constructor(private val firebaseApi: FirebaseApi,
                                         private val algoliaApiProvider: AlgoliaApiProvider) {

    fun getFavoriteList(userId: String, snapshotParser: SnapshotParser<ChatGroupInfo>) =
            firebaseApi.getIndexedChats(userId, snapshotParser)

    fun getPrivateList(userId: String, snapshotParser: SnapshotParser<ChatPrivateInfo>) =
            firebaseApi.getPrivateChats(userId, snapshotParser)

    fun toggleFavorite(userId: String, chatId: String) =
            firebaseApi.toggleFavorite(userId, chatId)

    fun <T>getChat(id: String, snapshotParser: SnapshotParser<T>) =
            firebaseApi.getChat(id, snapshotParser)

    fun getLogoReference(url: String) = firebaseApi.getLogoReference(url)

    fun query(term: String) = algoliaApiProvider.query(term)

    fun create(userId: String, chatGroup: ChatGroup): Completable {
        val key = firebaseApi.getChatKey()

        return Completable.merge(listOf(
                firebaseApi.setChat(chatGroup, key),
                firebaseApi.toggleFavorite(userId, key))
        )
    }

    fun createPrivate(currentUserId: String, chatPrivate: ChatPrivate): Single<String> {
        val key = firebaseApi.getChatKey()

        return Completable
                .merge(listOf(
                        firebaseApi.setChat(chatPrivate, key),
                        firebaseApi.togglePrivate(currentUserId, key),
                        firebaseApi.togglePrivate(chatPrivate.userId!!, key),
                        firebaseApi.toggleFavorite(chatPrivate.userId, key))
                ).toSingle { key }
    }

    fun update(chatId: String, chatName: String): Completable =
            firebaseApi.updateChat(chatId, chatName)

    fun updateLogo(chatId: String, chatLogo: Uri) =
            firebaseApi.updateChatLogo(chatId, chatLogo)

    fun observeChatList(snapshotParser: SnapshotParser<ChatGroupInfo>) =
            firebaseApi.getIndexedChats(snapshotParser)

    fun observeMessages(chatId: String, snapshotParser: SnapshotParser<Message>) =
            firebaseApi.getMessages(chatId, snapshotParser)
}