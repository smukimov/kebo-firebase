package com.kebo.kebo.models.data.storage

import android.content.Context
import javax.inject.Inject
import android.content.SharedPreferences
import com.kebo.kebo.BuildConfig

class TempDataRepository @Inject constructor(context: Context) {

    private val mPrefs: SharedPreferences

    init {
        this.mPrefs = context.getSharedPreferences(APP_PREFS_FILE_NAME, Context.MODE_PRIVATE)
    }

    fun saveMessageLengthLimit(limit: Int) {
        mPrefs.edit().putInt(FRIENDLY_MSG_LENGTH, limit).apply()
    }

    val messageLengthLimit: Int
        get() = mPrefs.getInt(FRIENDLY_MSG_LENGTH, BuildConfig.FIRABASE_MSG_LENGHT)

    companion object {

        private val APP_PREFS_FILE_NAME = "app_preferences"

        private val FRIENDLY_MSG_LENGTH = "app_friendly_msg_length"
    }

}