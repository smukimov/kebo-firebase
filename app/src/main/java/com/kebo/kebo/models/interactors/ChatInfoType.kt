package com.kebo.kebo.models.interactors

enum class ChatInfoType {
    GROUP,
    PRIVATE
}