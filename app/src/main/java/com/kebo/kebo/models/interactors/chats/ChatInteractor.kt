package com.kebo.kebo.models.interactors.chats

import com.firebase.ui.database.FirebaseArray
import com.firebase.ui.database.SnapshotParser
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.Message
import com.kebo.kebo.models.data.server.firebase.ChatGroupInfoSnapshotParser
import com.kebo.kebo.models.data.server.firebase.ChatPrivateInfoSnapshotParser
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.models.repository.ChatRepository
import io.reactivex.Flowable
import javax.inject.Inject

class ChatInteractor @Inject constructor(private val chatRepository: ChatRepository) {
    private fun getLogoReference(url: String): StorageReference =
            chatRepository.getLogoReference(url)

    fun observeChatGroup(id: String): Flowable<ChatGroupInfo> = chatRepository
            .getChat(id, ChatGroupInfoSnapshotParser(this::getLogoReference))

    fun observeChatPrivate(id: String): Flowable<ChatPrivateInfo> =
            chatRepository.getChat(id, ChatPrivateInfoSnapshotParser())


    fun observeMessages(chatId: String): FirebaseArray<Message> = chatRepository
            .observeMessages(chatId, SnapshotParser {
                it.getValue(Message::class.java)!!.copy(id = it.key)
            })
}