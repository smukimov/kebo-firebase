package com.kebo.kebo.models.data.server

import android.net.Uri
import com.firebase.ui.database.FirebaseArray
import com.firebase.ui.database.FirebaseIndexArray
import com.firebase.ui.database.SnapshotParser
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.UserProfile
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe

interface FirebaseApi {

    fun observeUserProfile(userId: String): Flowable<UserProfile>

    fun getUserProfile(userId: String): Maybe<UserProfile>

    fun <T> getIndexedChats(userId: String, snapshotParser: SnapshotParser<T>)
            : FirebaseIndexArray<T>

    fun <T> getIndexedChats(parser: SnapshotParser<T>): FirebaseIndexArray<T>

    fun <T> getPrivateChats(userId: String, parser: SnapshotParser<T>): FirebaseIndexArray<T>

    fun <T> getMessages(chatId: String, parser: SnapshotParser<T>): FirebaseArray<T>

    fun getChatKey(): String

    fun <T> getChat(chatId: String, parser: SnapshotParser<T>): Flowable<T>

    fun getLogoReference(url: String): StorageReference

    fun toggleFavorite(userId: String, chatId: String): Completable

    fun togglePrivate(userId: String, chatId: String): Completable

    fun setChat(chat: Any, key: String): Completable

    fun updateChat(chatId: String, chatName: String): Completable

    fun updateChatLogo(chatId: String, chatLogo: Uri): Completable

}