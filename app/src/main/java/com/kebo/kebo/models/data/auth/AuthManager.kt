package com.kebo.kebo.models.data.auth

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import javax.inject.Inject

class AuthManager @Inject constructor(val currentUser: FirebaseUser,
                                      private val firebaseAuth: FirebaseAuth) {
    fun signOut() {
        firebaseAuth.signOut()
    }
}