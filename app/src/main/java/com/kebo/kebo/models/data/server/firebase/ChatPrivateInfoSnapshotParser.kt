package com.kebo.kebo.models.data.server.firebase

import com.firebase.ui.database.SnapshotParser
import com.google.firebase.database.DataSnapshot
import com.kebo.kebo.entities.ChatPrivate
import com.kebo.kebo.models.interactors.ChatPrivateInfo

class ChatPrivateInfoSnapshotParser : SnapshotParser<ChatPrivateInfo> {
    override fun parseSnapshot(snapshot: DataSnapshot): ChatPrivateInfo {
        val chatPrivate = snapshot.getValue(ChatPrivate::class.java)!!
        return ChatPrivateInfo(chatPrivate.copy(chatId = snapshot.key))
    }
}