package com.kebo.kebo.models.interactors.chats

import android.net.Uri
import com.kebo.kebo.models.repository.ChatRepository
import javax.inject.Inject

class ChatUpdateInteractor
    @Inject constructor(private val chatRepository: ChatRepository) {

    fun update(chatId: String, chatName: String) =
            chatRepository.update(chatId, chatName)

    fun updateLogo(chatId: String, chatLogo: Uri) =
            chatRepository.updateLogo(chatId, chatLogo)
}