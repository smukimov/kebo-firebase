package com.kebo.kebo.models.interactors

import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.SearchResult

data class HitInfo(val hit: SearchResult.Hit, val logoURLReference: StorageReference? = null) {

    fun hasLogo() = logoURLReference != null

}