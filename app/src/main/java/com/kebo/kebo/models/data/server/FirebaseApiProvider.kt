package com.kebo.kebo.models.data.server

import android.net.Uri
import com.firebase.ui.database.FirebaseArray
import com.firebase.ui.database.FirebaseIndexArray
import com.firebase.ui.database.SnapshotParser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.ChatGroup
import com.kebo.kebo.entities.UserProfile
import durdinapps.rxfirebase2.RxFirebaseDatabase
import durdinapps.rxfirebase2.RxFirebaseStorage
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import mu.KLogging
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class FirebaseApiProvider
@Inject constructor(private val firebaseDatabase: FirebaseDatabase,
                    @Named("LogoStorage")
                    private val logosStorage: FirebaseStorage) : FirebaseApi {

    override fun observeUserProfile(userId: String): Flowable<UserProfile> {
        val q: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_USER_PROFILE).child(userId)

        return RxFirebaseDatabase.observeValueEvent(q).map {
            it.getValue(UserProfile::class.java)!!.copy(id = it.key)
        }.onErrorReturn { UserProfile().copy(id = userId) }
    }

    override fun getUserProfile(userId: String): Maybe<UserProfile> {
        val q: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_USER_PROFILE).child(userId)

        return RxFirebaseDatabase.observeSingleValueEvent(q).map {
            it.getValue(UserProfile::class.java)!!.copy(id = it.key)
        }
    }

    override fun <T> getIndexedChats(userId: String, snapshotParser: SnapshotParser<T>)
            : FirebaseIndexArray<T> {
        val keyQuery: Query = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS_FAVORITE)
                .child(userId)
        val dataRef: DatabaseReference = firebaseDatabase.reference.child(FIREBASE_CHILD_KEY_CHATS)
        return FirebaseIndexArray(keyQuery, dataRef, snapshotParser)
    }

    override fun <T> getIndexedChats(parser: SnapshotParser<T>): FirebaseIndexArray<T> {
        val keyQuery: Query = firebaseDatabase.reference.child(FIREBASE_CHILD_KEY_CHATS_INTRO)
        val dataRef: DatabaseReference = firebaseDatabase.reference.child(FIREBASE_CHILD_KEY_CHATS)
        return FirebaseIndexArray(keyQuery, dataRef, parser)
    }

    override fun <T> getPrivateChats(userId: String, parser: SnapshotParser<T>)
            : FirebaseIndexArray<T> {
        val keyQuery: Query = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS_PRIVATE)
                .child(userId)
        val dataRef: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS)
        return FirebaseIndexArray(keyQuery, dataRef, parser)
    }

    override fun <T> getChat(chatId: String, parser: SnapshotParser<T>): Flowable<T> {
        val q: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS).child(chatId)
        return RxFirebaseDatabase.observeValueEvent(q).map { parser.parseSnapshot(it) }
    }

    override fun getChatKey(): String =
            firebaseDatabase.reference.child(FIREBASE_CHILD_KEY_CHATS).push().key

    override fun setChat(chat: Any, key: String): Completable {
        val q: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS).child(key)
        return RxFirebaseDatabase.setValue(q, chat)
    }

    override fun updateChat(chatId: String, chatName: String): Completable {
        val chatRef: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS)
                .child(chatId)
                .child(FIREBASE_CHILD_KEY_CHATS_NAME)

        return RxFirebaseDatabase.setValue(chatRef, chatName)
    }

    override fun updateChatLogo(chatId: String, chatLogo: Uri)
            : Completable {

        val fileRef: StorageReference = logosStorage
                .reference
                .child(chatId)
                .child(UUID.randomUUID().toString())

        return  RxFirebaseStorage.putFile(fileRef, chatLogo).flatMapCompletable {
            Completable.complete()
        }
    }

    override fun getLogoReference(url: String) = logosStorage.reference.child(url)

    override fun toggleFavorite(userId: String, chatId: String): Completable {
        val userIdChatIdRef: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS_FAVORITE)
                .child(userId).child(chatId)

        val chatIdRef: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS)
                .child(chatId)

        return Completable.merge(listOf(
                Completable.create {
                    chatIdRef.runTransaction(object : Transaction.Handler {
                        override fun onComplete(e: DatabaseError?, b: Boolean, d: DataSnapshot?) {
                            logger.debug { "chatTransaction:onComplete:$e" }
                            if (e != null) {
                                it.onError(e.toException())
                            } else {
                                it.onComplete()
                            }
                        }

                        override fun doTransaction(mutableData: MutableData): Transaction.Result {
                            val chat = mutableData.getValue(ChatGroup::class.java)
                                    ?: return Transaction.success(mutableData)
                            chat.toggleFavorite(userId)
                            mutableData.value = chat

                            return Transaction.success(mutableData)
                        }
                    })
                },
                RxFirebaseDatabase.setValue(userIdChatIdRef, true)
        ))
    }

    override fun togglePrivate(userId: String, chatId: String): Completable {
        val currentUserIdToChatId: DatabaseReference = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_CHATS_PRIVATE)
                .child(userId).child(chatId)
        return RxFirebaseDatabase.setValue(currentUserIdToChatId, true)
    }

    override fun <T> getMessages(chatId: String, parser: SnapshotParser<T>): FirebaseArray<T> {
        val q: Query = firebaseDatabase.reference
                .child(FIREBASE_CHILD_KEY_MESSAGES)
                .child(chatId)

        return FirebaseArray<T>(q, parser)
    }

    companion object : KLogging() {
        private const val FIREBASE_CHILD_KEY_USER_PROFILE = "user-profile"
        private const val FIREBASE_CHILD_KEY_CHATS_FAVORITE = "chats-favorites"
        private const val FIREBASE_CHILD_KEY_CHATS_INTRO = "chats-intro"
        private const val FIREBASE_CHILD_KEY_CHATS_PRIVATE = "chats-private"
        private const val FIREBASE_CHILD_KEY_CHATS = "chats"
        private const val FIREBASE_CHILD_KEY_CHATS_NAME = "name"
        private const val FIREBASE_CHILD_KEY_MESSAGES = "messages"
    }
}