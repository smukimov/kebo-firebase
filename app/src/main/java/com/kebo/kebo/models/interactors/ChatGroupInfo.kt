package com.kebo.kebo.models.interactors

import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.ChatGroup

data class ChatGroupInfo(val chatGroup: ChatGroup, val logoURLReference: StorageReference? = null) {
    fun hasLogo() = logoURLReference != null
    fun id() = chatGroup.id!!

    val name: String
        get() = chatGroup.name

    val favoriteCount: String
        get() = "${chatGroup.favoriteCount} участников"

    val favorites: MutableMap<String, Boolean>
        get() = chatGroup.favorites

    val address: String
        get() = chatGroup.address

    companion object {
        fun parseData(data: Map<FieldName, String>) =
                ChatGroupInfo(ChatGroup(name = data[FieldName.NAME]!!))
    }

    enum class FieldName {
        NAME,
        FAVORITE_COUNT,
    }
}