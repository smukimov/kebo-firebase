package com.kebo.kebo.models.data.server

import com.algolia.search.saas.Client
import com.algolia.search.saas.Query
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import com.kebo.kebo.entities.Algolia
import com.kebo.kebo.entities.SearchResult
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Single
import io.reactivex.SingleObserver
import javax.inject.Inject

class AlgoliaApiProvider
    @Inject constructor(private val firebaseDatabase: FirebaseDatabase) : AlgoliaApi {

    override fun query(term: String): Single<SearchResult> {

        return object : Single<SearchResult>() {
            override fun subscribeActual(observer: SingleObserver<in SearchResult>) {
                val firebaseQuery = firebaseDatabase.reference.child(FIREBASE_ALGOLIA_KEY)

                RxFirebaseDatabase.observeSingleValueEvent(firebaseQuery, Algolia::class.java)
                        .subscribe { algolia: Algolia ->
                            val client = Client(algolia.appId!!, algolia.apiKey!!)
                            val index = client.getIndex(ALGOLIA_INDEX_KEY)

                            index.searchAsync(Query(term), { jsonObject, e ->
                                if (jsonObject != null && e == null) {
                                    val searchResult = Gson().fromJson(
                                            jsonObject.toString(), SearchResult::class.java)
                                    observer.onSuccess(searchResult)
                                } else {
                                    observer.onError(e)
                                }
                            })
                        }
            }
        }
    }

    companion object {
        private const val FIREBASE_ALGOLIA_KEY = "algolia"
        private const val ALGOLIA_INDEX_KEY = "chats"
    }

}