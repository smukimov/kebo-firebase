package com.kebo.kebo.models.data.server.firebase

import com.firebase.ui.database.SnapshotParser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.ChatGroup
import com.kebo.kebo.models.interactors.ChatGroupInfo

class ChatGroupInfoSnapshotParser(private val getLogoReference: (String) -> StorageReference)
    : SnapshotParser<ChatGroupInfo> {

    override fun parseSnapshot(snapshot: DataSnapshot): ChatGroupInfo {
        val chat = snapshot.getValue(ChatGroup::class.java)!!
        val logoURLReference =
                if (chat.logoURL != null) getLogoReference(chat.logoURL)
                else null
        return ChatGroupInfo(chat.copy(id = snapshot.key), logoURLReference)
    }
}