package com.kebo.kebo.models.interactors.chats

import com.google.firebase.auth.FirebaseUser
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.models.repository.ChatRepository
import io.reactivex.Single
import javax.inject.Inject

class ChatCreateInteractor
    @Inject constructor(private val chatRepository: ChatRepository,
                        private val currentUser: FirebaseUser) {

    fun create(chatGroupInfo: ChatGroupInfo) =
            chatRepository.create(currentUser.uid, chatGroupInfo.chatGroup)

    fun createPrivate(chatPrivateInfo: ChatPrivateInfo): Single<String> =
            chatRepository.createPrivate(currentUser.uid, chatPrivateInfo.chatPrivate)
}
