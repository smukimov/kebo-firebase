package com.kebo.kebo.models.interactors

import com.google.firebase.storage.FirebaseStorage
import com.kebo.kebo.entities.Message

data class MessageInfo(val message: Message) {

    fun isImageInStorage() = message.imageUrl!!.startsWith(FIREBASE_STORAGE_PROTOCOL)

    fun imageRef() = FirebaseStorage
            .getInstance()
            .reference
            .child(message.imageUrl!!)

    fun imageUrl(): String = message.imageUrl!!

    fun hasImageUrl() = !message.imageUrl.isNullOrEmpty()

    fun text() = message.text

    fun userId(): String = message.userId!!

    companion object {
        private val FIREBASE_STORAGE_PROTOCOL = "gs://"

        fun create(text: String, uid: String) =
                MessageInfo(Message(text = text, userId = uid))
    }
}