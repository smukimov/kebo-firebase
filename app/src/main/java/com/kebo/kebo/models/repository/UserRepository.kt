package com.kebo.kebo.models.repository

import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.models.data.server.FirebaseApi
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserProfileRepository @Inject constructor(private val firebaseApi: FirebaseApi) {

    fun observeUserProfile(userId: String): Flowable<UserProfile> = firebaseApi
            .observeUserProfile(userId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())

    fun getUserProfile(userId: String) = firebaseApi.getUserProfile(userId)

}