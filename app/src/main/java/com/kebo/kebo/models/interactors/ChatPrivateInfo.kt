package com.kebo.kebo.models.interactors

import com.kebo.kebo.entities.ChatPrivate
import com.kebo.kebo.entities.UserProfile

class ChatPrivateInfo(val chatPrivate: ChatPrivate) {

    fun chatId() = chatPrivate.chatId!!

    fun getPeer(currentUserId: String): String {
        var peerId = ""
        chatPrivate.favorites.forEach {
            if (it.key != currentUserId) {
                peerId = it.key
            }
        }
        return peerId
    }

    companion object {
        fun create(userProfile: UserProfile) =
                ChatPrivateInfo(ChatPrivate(userProfile.id))

        fun create(messageInfo: MessageInfo) =
            ChatPrivateInfo(ChatPrivate(messageInfo.userId()))
    }
}