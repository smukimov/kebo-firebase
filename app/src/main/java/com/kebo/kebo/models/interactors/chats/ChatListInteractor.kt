package com.kebo.kebo.models.interactors.chats

import com.kebo.kebo.models.data.auth.AuthManager
import com.kebo.kebo.models.data.server.firebase.ChatGroupInfoSnapshotParser
import com.kebo.kebo.models.data.server.firebase.ChatPrivateInfoSnapshotParser
import com.kebo.kebo.models.interactors.HitInfo
import com.kebo.kebo.models.repository.ChatRepository
import io.reactivex.Single
import javax.inject.Inject

class ChatListInteractor
@Inject constructor(private val chatRepository: ChatRepository,
                    private val authManager: AuthManager) {

    fun searchChatByTerm(term: String): Single<List<HitInfo>> = chatRepository
            .query(term)
            .map {
                it.hits?.map {
                    if (it.logoURL.isNullOrEmpty()) HitInfo(it)
                    else HitInfo(it, getLogoReference(it.logoURL!!))
                }
            }

    fun addToFavoriteList(userId: String, chatId: String) =
            chatRepository.toggleFavorite(userId, chatId)

    fun observeFavoriteList() = chatRepository.getFavoriteList(authManager.currentUser.uid,
            ChatGroupInfoSnapshotParser(this::getLogoReference))

    fun observeIntroList() = chatRepository
            .observeChatList(ChatGroupInfoSnapshotParser(this::getLogoReference))

    fun observePrivateList() = chatRepository
            .getPrivateList(authManager.currentUser.uid, ChatPrivateInfoSnapshotParser())

    private fun getLogoReference(url: String) = chatRepository.getLogoReference(url)
}