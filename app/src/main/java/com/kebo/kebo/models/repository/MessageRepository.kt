package com.kebo.kebo.models.repository

import android.net.Uri
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.Message
import com.kebo.kebo.models.interactors.MessageInfo
import durdinapps.rxfirebase2.RxFirebaseStorage
import javax.inject.Inject
import javax.inject.Named

class MessageRepository
    @Inject constructor (private val firebaseDatabase: FirebaseDatabase,
                         private val firebaseAnalytics: FirebaseAnalytics,
                         @Named("DefaultStorage") private val firebaseStorage: FirebaseStorage) {

    fun saveImage(chatId: String, userId: String, imageUrl: Uri) {
        val firebaseDBReference = firebaseDatabase.reference!!

        val message = Message(userId = userId, imageUrl = LOADING_IMAGE_URL)

        val messagePath = firebaseDBReference
                .child(FIREBASE_CHILD_KEY_MESSAGES)
                .child(chatId)
                .push()

        val completeListener = DatabaseReference.CompletionListener { error, _ ->
            if (error == null) {
                val storageReference = firebaseStorage.getReference(userId)
                        .child(messagePath.key)
                        .child(imageUrl.lastPathSegment)
                RxFirebaseStorage
                        .putFile(storageReference, imageUrl)
                        .subscribe({
                            messagePath.child("imageUrl").setValue(it.downloadUrl!!.toString())
                        })
            }
        }

        messagePath.setValue(message, completeListener)
    }

    fun getImageReference(imageUrl: String): StorageReference =
            firebaseStorage.getReferenceFromUrl(imageUrl)

    fun saveMessage(chatId: String, messageInfo: MessageInfo) {
        val message = messageInfo.message
        firebaseDatabase.reference.child(FIREBASE_CHILD_KEY_MESSAGES).child(chatId)
                .push()
                .setValue(message)
        firebaseAnalytics.logEvent(MESSAGE_SENT_EVENT, null)
    }

    fun databaseReference(chatId: String) = firebaseDatabase
            .reference.child(FIREBASE_CHILD_KEY_MESSAGES).child(chatId)!!

    companion object {
        private const val LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif"
        private const val FIREBASE_CHILD_KEY_MESSAGES = "messages"
        private const val MESSAGE_SENT_EVENT = "message_sent"
    }
}