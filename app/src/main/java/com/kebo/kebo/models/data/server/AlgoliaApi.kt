package com.kebo.kebo.models.data.server

import com.kebo.kebo.entities.SearchResult
import io.reactivex.Single

interface AlgoliaApi {
    fun query(term: String): Single<SearchResult>
}