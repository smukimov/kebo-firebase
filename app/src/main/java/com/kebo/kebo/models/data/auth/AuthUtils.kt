package com.kebo.kebo.models.data.auth

object AuthUtils {
    fun isEmailValid(email: String): Boolean = email.contains("@")

    fun isPasswordValid(password: String): Boolean {
        var hasLetter = false
        var hasDigit = false

        if (password.length >= 8) {
            for (i in 0 until password.length) {
                val x = password[i]
                if (Character.isLetter(x)) {
                    hasLetter = true
                } else if (Character.isDigit(x)) {
                    hasDigit = true
                }

                if (hasLetter && hasDigit) {
                    break
                }

            }
            return hasLetter && hasDigit
        }

        return false
    }

    fun usernameFromEmail(email: String): String {
        return if (email.contains("@")) {
            email.split("@".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
        } else {
            email
        }
    }
}