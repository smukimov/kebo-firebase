package com.kebo.kebo.presentation.drawer

import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.dagger.FragmentScoped
import com.kebo.kebo.ui.drawer.NavigationDrawerFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class NavigationDrawerModule {
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun navigationDrawerFragment(): NavigationDrawerFragment

    @ActivityScoped
    @Binds
    internal abstract fun bindNavigationDrawerPresenter(presenter: NavigationDrawerPresenter):
            NavigationDrawerContract.Presenter
}