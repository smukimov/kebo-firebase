package com.kebo.kebo.presentation.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.firebase.auth.*
import javax.inject.Inject
import com.google.firebase.auth.PhoneAuthCredential

@InjectViewState
class PhoneAuthPresenter @Inject constructor(private val auth: FirebaseAuth)
    : MvpPresenter<AuthContract.PhoneAuthView>(), AuthContract.PhoneAuthPresenter {

    override fun signInWithCredential(credential: PhoneAuthCredential) =
            auth.signInWithCredential(credential)
}