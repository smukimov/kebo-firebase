package com.kebo.kebo.presentation.settings

import com.arellomobile.mvp.MvpView
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.presentation.global.BasePresenter

class SettingsContract {
    interface View : MvpView {
        fun showUserProfile(userProfile: UserProfile)
        fun openFileChooserDialog()
        fun showToast(id: Int)
    }

    interface Presenter : BasePresenter<View> {
        fun retrieveUserProfile()
        fun onAvatarClicked(granted: Boolean)
    }
}