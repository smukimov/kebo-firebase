package com.kebo.kebo.presentation.chats.details

import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.google.firebase.auth.FirebaseAuth
import com.kebo.kebo.models.interactors.chats.ChatInteractor
import com.kebo.kebo.models.repository.UserProfileRepository
import io.reactivex.Completable
import javax.inject.Inject

@InjectViewState
class ChatPrivateDetailsPresenter
    @Inject constructor(
            private val auth: FirebaseAuth,
            private val userProfileRepository: UserProfileRepository,
        private val chatInteractor: ChatInteractor)
    : ChatDetailsPresenter() {

    override fun getUserProfile(userId: String) = userProfileRepository.getUserProfile(userId)

    override fun getCurrentUserId(): String = auth.currentUser!!.uid

    override fun setLogo(uri: Uri): Completable = Completable.complete()

    override fun initChat(){
        chatInteractor.observeChatPrivate(chatId).subscribe {
            viewState.setChatPrivateInfo(it)
        }
    }
}