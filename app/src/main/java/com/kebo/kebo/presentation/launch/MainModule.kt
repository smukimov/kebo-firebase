package com.kebo.kebo.presentation.launch

import com.kebo.kebo.ScreenType
import com.kebo.kebo.dagger.*
import dagger.Module
import dagger.Binds
import com.kebo.kebo.entities.MessageType
import com.kebo.kebo.models.interactors.ChatInfoType
import com.kebo.kebo.presentation.chats.list.intros.ChatIntroListContract
import com.kebo.kebo.presentation.chats.list.intros.ChatIntroListPresenter
import com.kebo.kebo.presentation.chats.list.favorites.ChatFavoriteListContract
import com.kebo.kebo.presentation.chats.list.favorites.ChatFavoriteListPresenter
import com.kebo.kebo.presentation.chats.list.privates.ChatPrivateListContract
import com.kebo.kebo.presentation.chats.list.privates.ChatPrivateListPresenter
import com.kebo.kebo.ui.chats.create.ChatCreateFragment
import com.kebo.kebo.ui.chats.create.NavigationChatCreateFactory
import com.kebo.kebo.ui.chats.edit.ChatEditFragment
import com.kebo.kebo.ui.chats.edit.NavigationChatEditFactory
import com.kebo.kebo.ui.chats.list.*
import com.kebo.kebo.ui.chats.list.navigation.NavigationChatFavoriteListFactory
import com.kebo.kebo.ui.chats.list.navigation.NavigationChatIntroListFactory
import com.kebo.kebo.ui.chats.list.navigation.NavigationChatPrivateListFactory
import com.kebo.kebo.ui.chats.list.viewholder.ChatViewHolderGroupFactory
import com.kebo.kebo.ui.chats.list.viewholder.ChatViewHolderPrivateFactory
import com.kebo.kebo.ui.chats.show.*
import com.kebo.kebo.ui.chats.show.navigation.*
import com.kebo.kebo.ui.chats.show.viewholder.MessageViewHolderImageFactory
import com.kebo.kebo.ui.chats.show.viewholder.MessageViewHolderTextFactory
import com.kebo.kebo.ui.global.navigation.NavigationDummyFactory
import com.kebo.kebo.ui.global.navigation.NavigationFactory
import com.kebo.kebo.ui.global.viewholder.ViewHolderFactory
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MainModule {
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chatListFragment(): ChatIntroListFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chatFragment(): ChatFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chatCreateFragment(): ChatCreateFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chatDetailsFragment(): ChatDetailsFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chatEditFragment(): ChatEditFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chatFavoriteListFragment(): ChatFavoriteListFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun chatPrivateListFragment(): ChatPrivateListFragment

    @ActivityScoped
    @Binds
    abstract fun bindChatListListPresenter(chatIntroListPresenter: ChatIntroListPresenter):
            ChatIntroListContract.Presenter

    @ActivityScoped
    @Binds
    abstract fun bindChatFavoriteListPresenter(chatFavoriteListPresenter: ChatFavoriteListPresenter):
            ChatFavoriteListContract.Presenter

    @ActivityScoped
    @Binds
    abstract fun bindChatPrivateListPresenter(chatPrivateListPresenter: ChatPrivateListPresenter):
            ChatPrivateListContract.Presenter

    @ActivityScoped
    @Binds
    abstract fun bindLaunchPresenter(presenter: LaunchPresenter): LaunchContract.Presenter

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHATS_INTRO)
    abstract fun bindNavigationChatIntroList
            (navigationChatIntroListFactory: NavigationChatIntroListFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHAT_GROUP)
    abstract fun bindNavigationChatGroupShow
            (navigationChatGroupFactory: NavigationChatGroupFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHAT_PRIVATE)
    abstract fun bindNavigationChatPrivateShow
            (navigationChatPrivateFactory: NavigationChatPrivateFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHATS_FAVORITES)
    abstract fun bindNavigationChatFavoriteList
            (navigationChatFavoriteListFactory: NavigationChatFavoriteListFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHAT_CREATE)
    abstract fun bindNavigationChatCreate
            (navigationchatCreateFactory: NavigationChatCreateFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHAT_GROUP_DETAILS)
    abstract fun bindNavigationChatGroupDetails
            (navigationChatGroupDetailsFactory: NavigationChatGroupDetailsFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHAT_PRIVATE_DETAILS)
    abstract fun bindNavigationChatPrivateDetails
            (navigationChatPrivateDetailsFactory: NavigationChatPrivateDetailsFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHAT_EDIT)
    abstract fun bindNavigationChatEdit
            (navigationChatEditFactory: NavigationChatEditFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.DUMMY)
    abstract fun bindNavigationDummy
            (navigationDummyFactory: NavigationDummyFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ScreenKey(ScreenType.CHATS_PRIVATE)
    abstract fun bindNavigationChatPrivateList
            (navigationChatPrivateListFactory: NavigationChatPrivateListFactory): NavigationFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @ChatTypeKey(ChatInfoType.GROUP)
    abstract fun bindChatViewHolderGroup
            (chatViewHolderNormalFactory: ChatViewHolderGroupFactory): ViewHolderFactory
    @ActivityScoped
    @Binds
    @IntoMap
    @ChatTypeKey(ChatInfoType.PRIVATE)
    abstract fun bindChatViewHolderPrivate
            (chatViewHolderNormalFactory: ChatViewHolderPrivateFactory): ViewHolderFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @MessageTypeKey(MessageType.TEXT)
    abstract fun bindMessageViewHolderText
            (messageViewHolderTextFactory: MessageViewHolderTextFactory): ViewHolderFactory

    @ActivityScoped
    @Binds
    @IntoMap
    @MessageTypeKey(MessageType.IMAGE)
    abstract fun bindMessageViewHolderImage
            (messageViewHolderImageFactory: MessageViewHolderImageFactory): ViewHolderFactory

}