package com.kebo.kebo.presentation.launch

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.newRootScreen
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class LaunchPresenter
    @Inject constructor(private val router: Router) : MvpPresenter<LaunchContract.View>(),
        LaunchContract.Presenter {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        router.newRootScreen(ScreenType.CHATS_INTRO)
    }

    override fun onBackPressed() = router.finishChain()
}