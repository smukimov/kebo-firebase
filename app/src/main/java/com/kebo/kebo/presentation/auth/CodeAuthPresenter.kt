package com.kebo.kebo.presentation.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import javax.inject.Inject

@InjectViewState
class CodeAuthPresenter @Inject constructor(private val auth: FirebaseAuth)
    : MvpPresenter<AuthContract.CodeAuthView>(), AuthContract.CodeAuthPresenter {

    override fun verifyCode(verificationId: String, code: String) {
        val credential = PhoneAuthProvider.getCredential(verificationId, code)
        viewState.verifyCodeCompleted(auth.signInWithCredential(credential))
    }
}
