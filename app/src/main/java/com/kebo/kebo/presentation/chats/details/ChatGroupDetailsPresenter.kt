package com.kebo.kebo.presentation.chats.details

import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.models.interactors.chats.ChatInteractor
import com.kebo.kebo.models.interactors.chats.ChatUpdateInteractor
import io.reactivex.Maybe
import javax.inject.Inject

@InjectViewState
class ChatGroupDetailsPresenter
    @Inject constructor(
        private val chatUpdateInteractor: ChatUpdateInteractor,
        private val chatInteractor: ChatInteractor)
    : ChatDetailsPresenter() {

    override fun getUserProfile(userId: String): Maybe<UserProfile> {
        TODO("not implemented")
    }

    override fun getCurrentUserId(): String {
        TODO("not implemented")
    }

    override fun setLogo(uri: Uri) = chatUpdateInteractor.updateLogo(chatId, uri)

    override fun initChat(){
        chatInteractor.observeChatGroup(chatId).subscribe {
            viewState.setChatGroupInfo(it)
        }
    }
}