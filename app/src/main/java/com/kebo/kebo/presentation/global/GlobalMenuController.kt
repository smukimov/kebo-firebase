package com.kebo.kebo.presentation.global

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GlobalMenuController @Inject constructor() {
    private val stateRelay = BehaviorRelay.createDefault(false)

    val state: Observable<Boolean> = stateRelay
    fun open() = stateRelay.accept(true)
    fun close() = stateRelay.accept(false)
}
