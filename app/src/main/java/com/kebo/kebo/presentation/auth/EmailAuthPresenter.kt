package com.kebo.kebo.presentation.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.google.firebase.auth.FirebaseAuth
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.navigateTo
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class EmailAuthPresenter
    @Inject() constructor(private val auth: FirebaseAuth, private val router: Router)
    : MvpPresenter<MvpView>(), AuthContract.EmailAuthPresenter {

    override fun signInWithEmailAndPassword(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                router.navigateTo(ScreenType.CHATS_INTRO)
            } else {
                router.showSystemMessage("Authentication failed.")
            }
        }
    }
}