package com.kebo.kebo.presentation.chats.edit

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kebo.kebo.models.interactors.chats.ChatUpdateInteractor
import com.kebo.kebo.models.interactors.chats.ChatInteractor
import javax.inject.Inject

@InjectViewState
class ChatEditPresenter @Inject constructor(
        private val chatUpdateInteractor: ChatUpdateInteractor,
        private val chatInteractor: ChatInteractor)
    : ChatEditContract.Presenter, MvpPresenter<ChatEditContract.View>() {

    override fun updateChatName(chatId: String, chatName: String) =
        chatUpdateInteractor.update(chatId, chatName)

    override fun initChatToEdit(chatId: String){
        chatInteractor.observeChatGroup(chatId).subscribe {
            viewState.setChatInfoToEdit(it)
        }
    }

}