package com.kebo.kebo.presentation.chats.create

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kebo.kebo.ScreenType
import com.kebo.kebo.models.interactors.chats.ChatCreateInteractor
import com.kebo.kebo.models.interactors.ChatGroupInfo
import ru.terrakok.cicerone.Router
import com.kebo.kebo.extension.navigateTo
import javax.inject.Inject

@InjectViewState
class ChatCreatePresenter
    @Inject constructor(private val router: Router,
                    private val chatCreateInteractor: ChatCreateInteractor)
    : MvpPresenter<ChatCreateContract.View>(), ChatCreateContract.Presenter {

    fun onBackPressed() {
        router.exit()
    }

    override fun create(data: Map<ChatGroupInfo.FieldName, String>) {
        val chatInfo = ChatGroupInfo.parseData(data)
        chatCreateInteractor.create(chatInfo).subscribe {
            router.navigateTo(ScreenType.CHATS_FAVORITES)
        }
    }
}