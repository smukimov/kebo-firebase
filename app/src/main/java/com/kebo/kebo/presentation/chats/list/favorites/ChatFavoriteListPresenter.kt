package com.kebo.kebo.presentation.chats.list.favorites

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.models.interactors.chats.ChatListInteractor
import mu.KLogging
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ChatFavoriteListPresenter
    @Inject constructor(private val router: Router,
                        private val chatListInteractor: ChatListInteractor)
    : MvpPresenter<ChatFavoriteListContract.View>(), ChatFavoriteListContract.Presenter {

    fun onBackPressed() = router.exit()

    override fun onChatClicked(chatId: String) {
        router.navigateTo(ScreenType.CHAT_GROUP, chatId)
    }

    override fun observeFavoriteList() = chatListInteractor.observeFavoriteList()

    companion object: KLogging()
}