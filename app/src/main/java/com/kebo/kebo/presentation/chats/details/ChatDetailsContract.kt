package com.kebo.kebo.presentation.chats.details

import android.net.Uri
import com.arellomobile.mvp.MvpView
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import io.reactivex.Completable
import io.reactivex.Maybe

interface ChatDetailsContract {
    interface View : MvpView {

        fun setChatGroupInfo(chatGroupInfo: ChatGroupInfo)

        fun setChatPrivateInfo(chatPrivateInfo: ChatPrivateInfo)

        fun showLogo(reference: StorageReference)

    }

    interface Presenter {

        val chatId: String

        fun initChat()

        fun setLogo(uri: Uri): Completable

        fun getUserProfile(userId: String): Maybe<UserProfile>

        fun getCurrentUserId(): String
    }
}