package com.kebo.kebo.presentation.chats.edit

import com.arellomobile.mvp.MvpView
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.presentation.global.BasePresenter
import io.reactivex.Completable

interface ChatEditContract {
    interface View : MvpView {
        fun setChatInfoToEdit(chatGroupInfo: ChatGroupInfo)
    }

    interface Presenter : BasePresenter<View> {
        fun initChatToEdit(chatId: String)

        fun updateChatName(chatId: String, chatName: String): Completable
    }
}