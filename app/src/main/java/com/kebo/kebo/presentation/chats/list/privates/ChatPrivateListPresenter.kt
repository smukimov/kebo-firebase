package com.kebo.kebo.presentation.chats.list.privates

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.firebase.ui.database.FirebaseIndexArray
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.kebo.kebo.ScreenType
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.models.interactors.chats.ChatListInteractor
import com.kebo.kebo.models.repository.UserProfileRepository
import io.reactivex.Flowable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ChatPrivateListPresenter @Inject constructor(private val router: Router,
                                                   private val auth: FirebaseAuth,
                                                   private val userProfileRepository: UserProfileRepository,
                                                   private val interactor: ChatListInteractor)
    : MvpPresenter<ChatPrivateListContract.View>(), ChatPrivateListContract.Presenter {

    fun onBackPressed() = router.exit()

    override fun observePrivateList(): FirebaseIndexArray<ChatPrivateInfo> =
            interactor.observePrivateList()

    override fun observeUserProfile(userId: String): Flowable<UserProfile> =
            userProfileRepository.observeUserProfile(userId)

    override fun currentUserId(): String = auth.currentUser!!.uid

    override fun onChatClicked(chatId: String) {
        router.navigateTo(ScreenType.CHAT_PRIVATE, chatId)
    }
}