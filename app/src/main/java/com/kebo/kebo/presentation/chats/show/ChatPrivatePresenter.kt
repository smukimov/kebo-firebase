package com.kebo.kebo.presentation.chats.show

import com.arellomobile.mvp.InjectViewState
import com.google.firebase.auth.FirebaseUser
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.models.data.storage.TempDataRepository
import com.kebo.kebo.models.interactors.chats.ChatInteractor
import com.kebo.kebo.models.repository.MessageRepository
import com.kebo.kebo.models.repository.UserProfileRepository
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ChatPrivatePresenter
@Inject constructor(messageRepository: MessageRepository,
                    private val chatInteractor: ChatInteractor,
                    private val userProfileRepository: UserProfileRepository,
                    private val currentUser: FirebaseUser,
                    router: Router,
                    tempDataRepository: TempDataRepository)
    : ChatPresenter(messageRepository, chatInteractor, userProfileRepository, currentUser, router,
        tempDataRepository) {

    private val disposables = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun initToolbar(chatId: String) {
        chatInteractor.observeChatPrivate(chatId)
                .subscribe { chatInfo ->
                    userProfileRepository.observeUserProfile(chatInfo.getPeer(currentUser.uid)).subscribe {
                        if (!it.displayName.isNullOrEmpty()) {
                            viewState.setChatName(it.displayName!!)
                        }

                        if (!it.photoURL.isNullOrEmpty()) {
                            viewState.setLogo(it.photoURLRef)
                        } else {
                            viewState.setLogoDefault()
                        }

                        viewState.chatJoined(true)
                    }
                }.addTo(disposables)
    }
}