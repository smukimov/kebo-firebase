package com.kebo.kebo.presentation.chats.show

import android.net.Uri
import com.arellomobile.mvp.MvpView
import com.firebase.ui.database.FirebaseArray
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.Message
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.models.interactors.MessageInfo
import io.reactivex.Flowable

interface ChatContract {
    interface View : MvpView {

        fun openImagePickDialog()

        fun clearMessageEditText()

        fun sendInvitation()

        fun toggleSendButton(showOrHide: Boolean)

        fun toggleAttachmentButton(showOrHide: Boolean)

        fun scrollToPosition(position: Int)

        fun setChatName(name: String)

        fun setMemberCount(memberCount: String)

        fun setLogo(logoRef: StorageReference)

        fun setLogoDefault()

        fun chatJoined(isJoined: Boolean)

    }

    interface Presenter {

        fun sendMessage(chatId: String, text: String)

        fun sendImage(chatId: String, uri: Uri)

        fun initToolbar(chatId: String)

        fun openImagePickDialog()

        fun getReference(chatId: String): DatabaseReference

        fun initUserProfile(userId: String): Flowable<UserProfile>

        fun getImageReference(imageUrl: String): StorageReference

        fun onMessageEditChanged(text: CharSequence)

        fun addToFavorites(chatId: String)

        fun messagePublisher(chatId: String): FirebaseArray<Message>

        fun onBackPressed()

        fun onAvatarClicked(messageInfo: MessageInfo)

        fun observeUserProfile(userId: String): Flowable<UserProfile>

        val messageLengthLimit: Int

    }
}