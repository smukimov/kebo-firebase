package com.kebo.kebo.presentation.chats.create

import com.arellomobile.mvp.MvpView
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.presentation.global.BasePresenter

interface ChatCreateContract {
    interface View : MvpView
    interface Presenter : BasePresenter<View> {
        fun create(data: Map<ChatGroupInfo.FieldName, String>)
    }
}