package com.kebo.kebo.presentation.chats.list.privates

import com.arellomobile.mvp.MvpView
import com.firebase.ui.database.FirebaseIndexArray
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import io.reactivex.Flowable

interface ChatPrivateListContract {
    interface View : MvpView

    interface Presenter {

        fun observePrivateList(): FirebaseIndexArray<ChatPrivateInfo>

        fun onChatClicked(chatId: String)

        fun observeUserProfile(userId: String): Flowable<UserProfile>

        fun currentUserId(): String
    }
}