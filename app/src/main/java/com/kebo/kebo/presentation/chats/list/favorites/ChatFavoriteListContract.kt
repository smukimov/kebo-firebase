package com.kebo.kebo.presentation.chats.list.favorites

import com.arellomobile.mvp.MvpView
import com.firebase.ui.database.FirebaseIndexArray
import com.kebo.kebo.models.interactors.ChatGroupInfo

interface ChatFavoriteListContract {

    interface View : MvpView

    interface Presenter {

        fun observeFavoriteList(): FirebaseIndexArray<ChatGroupInfo>

        fun onChatClicked(chatId: String)

    }

}