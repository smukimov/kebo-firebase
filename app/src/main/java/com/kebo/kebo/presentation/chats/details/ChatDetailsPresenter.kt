package com.kebo.kebo.presentation.chats.details

import com.arellomobile.mvp.MvpPresenter

abstract class ChatDetailsPresenter
    : ChatDetailsContract.Presenter, MvpPresenter<ChatDetailsContract.View>() {

    private var _chatId: String? = null

    override var chatId: String
        get() = _chatId!!
        set(value) {
            _chatId = value
        }
}