package com.kebo.kebo.presentation.auth

import com.arellomobile.mvp.MvpView
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.PhoneAuthCredential
import com.kebo.kebo.presentation.global.BasePresenter

class AuthContract {
    interface CodeAuthView : MvpView {
        fun verifyCodeCompleted(authResult: Task<AuthResult>)
    }

    interface PhoneAuthView : MvpView {
        fun verifyPhoneNumber(phoneNumber: String)
    }

    interface CodeAuthPresenter : BasePresenter<CodeAuthView> {
        fun verifyCode(verificationId: String, code: String)
    }

    interface PhoneAuthPresenter: BasePresenter<PhoneAuthView> {
        fun signInWithCredential(credential: PhoneAuthCredential): Task<AuthResult>
    }

    interface EmailAuthPresenter: BasePresenter<MvpView> {
        fun signInWithEmailAndPassword(email: String, password: String)
    }
}