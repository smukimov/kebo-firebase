package com.kebo.kebo.presentation.settings

import com.kebo.kebo.dagger.ActivityScoped
import com.kebo.kebo.dagger.FragmentScoped
import com.kebo.kebo.ui.settings.SettingsFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SettingsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun settingsFragment(): SettingsFragment

    @ActivityScoped
    @Binds
    internal abstract fun bindSettingsPresenter(presenter: SettingsPresenter):
            SettingsContract.Presenter
}