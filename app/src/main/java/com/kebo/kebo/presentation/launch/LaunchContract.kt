package com.kebo.kebo.presentation.launch

import com.arellomobile.mvp.MvpView
import com.kebo.kebo.presentation.global.BasePresenter
import com.kebo.kebo.presentation.global.BaseView

class LaunchContract {
    interface View : MvpView
    interface Presenter {
        fun onBackPressed()
    }
}