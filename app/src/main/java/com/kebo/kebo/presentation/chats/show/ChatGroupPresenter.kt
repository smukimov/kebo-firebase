package com.kebo.kebo.presentation.chats.show

import com.arellomobile.mvp.InjectViewState
import com.google.firebase.auth.FirebaseUser
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.models.data.storage.TempDataRepository
import com.kebo.kebo.models.interactors.ChatPrivateInfo
import com.kebo.kebo.models.interactors.MessageInfo
import com.kebo.kebo.models.interactors.chats.ChatCreateInteractor
import com.kebo.kebo.models.interactors.chats.ChatInteractor
import com.kebo.kebo.models.interactors.chats.ChatListInteractor
import com.kebo.kebo.models.repository.MessageRepository
import com.kebo.kebo.models.repository.UserProfileRepository
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ChatGroupPresenter
    @Inject constructor(messageRepository: MessageRepository,
                        private val chatInteractor: ChatInteractor,
                        private val chatListInteractor: ChatListInteractor,
                        private val chatCreateInteractor: ChatCreateInteractor,
                        userProfileRepository: UserProfileRepository,
                        private val currentUser: FirebaseUser,
                        private val router: Router,
                        tempDataRepository: TempDataRepository)
    : ChatPresenter(messageRepository, chatInteractor, userProfileRepository, currentUser, router,
        tempDataRepository) {

    private val disposables = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun initToolbar(chatId: String) {
        chatInteractor.observeChatGroup(chatId)
                .subscribe { chatInfo ->
                    viewState.apply {
                        setChatName(chatInfo.name)
                        setMemberCount(chatInfo.favoriteCount)
                        if (chatInfo.hasLogo()) {
                            setLogo(chatInfo.logoURLReference!!)
                        } else {
                            setLogoDefault()
                        }
                        chatJoined(chatInfo.favorites.containsKey(currentUser.uid))
                    }}.addTo(disposables)
    }

    override fun addToFavorites(chatId: String) {
        chatListInteractor.addToFavoriteList(currentUser.uid, chatId).subscribe {
            router.showSystemMessage("Этот чат был добавлен в избранное")
        }
    }


    override fun onAvatarClicked(messageInfo: MessageInfo) {
        val chatPrivateInfo = ChatPrivateInfo.create(messageInfo)
        chatCreateInteractor.createPrivate(chatPrivateInfo).subscribe { chatId ->
            router.navigateTo(ScreenType.CHAT_GROUP, chatId)
        }.addTo(disposables)
    }

    override fun onMessageEditChanged(text: CharSequence) {
        super.onMessageEditChanged(text)
        val showOrHide = text.trim { it <= ' ' }.isNotEmpty()
        viewState.toggleAttachmentButton(!showOrHide)
    }
}