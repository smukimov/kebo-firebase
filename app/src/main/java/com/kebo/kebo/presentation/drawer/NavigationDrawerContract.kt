package com.kebo.kebo.presentation.drawer

import com.arellomobile.mvp.MvpView
import com.kebo.kebo.presentation.global.BasePresenter
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.presentation.drawer.NavigationDrawerContract.View.MenuItem

interface NavigationDrawerContract {

    interface View : MvpView {
        enum class MenuItem {
            CHATS_PRIVATE,
            CHATS_INTRO,
            FAVORITES,
            SETTINGS,
            SIGN_OUT
        }

        fun showUserInfo(userProfile: UserProfile)
        fun onScreenChanged(menuItem: MenuItem)
        fun selectMenuItem(menuItem: MenuItem)
    }

    interface Presenter : BasePresenter<View> {
        fun showUserProfile()
        fun onScreenChanged(item: MenuItem)
        fun onMenuItemClick(item: MenuItem)
        fun signOut()
    }
}