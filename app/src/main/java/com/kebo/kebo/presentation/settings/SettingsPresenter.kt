package com.kebo.kebo.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kebo.kebo.R
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.models.data.auth.AuthManager
import com.kebo.kebo.models.repository.UserProfileRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@InjectViewState
class SettingsPresenter
    @Inject constructor(
            private val userProfileRepository: UserProfileRepository,
            private val authManager: AuthManager)
    : MvpPresenter<SettingsContract.View>(), SettingsContract.Presenter {

    private val disposable = CompositeDisposable()

    override fun retrieveUserProfile() {
        userProfileRepository.observeUserProfile(authManager.currentUser.uid)
                .subscribe { userProfile ->
                    viewState.showUserProfile(userProfile)
                }.addTo(disposable)

    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    override fun onAvatarClicked(granted: Boolean) {
        if (granted) {
            viewState.openFileChooserDialog()
        } else {
            viewState.showToast(R.string.cant_read_external_storage)
        }
    }
}