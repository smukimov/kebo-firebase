package com.kebo.kebo.presentation.chats.show

import android.net.Uri
import com.arellomobile.mvp.MvpPresenter
import com.firebase.ui.database.FirebaseArray
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.storage.StorageReference
import com.kebo.kebo.entities.Message
import com.kebo.kebo.entities.UserProfile
import com.kebo.kebo.models.repository.MessageRepository
import com.kebo.kebo.models.data.storage.TempDataRepository
import com.kebo.kebo.models.interactors.chats.ChatInteractor
import com.kebo.kebo.models.interactors.MessageInfo
import com.kebo.kebo.models.repository.UserProfileRepository
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import mu.KLogging
import ru.terrakok.cicerone.Router

abstract class ChatPresenter
constructor(private val messageRepository: MessageRepository,
            private val chatInteractor: ChatInteractor,
            private val userProfileRepository: UserProfileRepository,
            private val currentUser: FirebaseUser,
            private val router: Router,
            tempDataRepository: TempDataRepository)
    : MvpPresenter<ChatContract.View>(), ChatContract.Presenter {

    private val disposables = CompositeDisposable()

    override val messageLengthLimit: Int = tempDataRepository.messageLengthLimit

    override fun sendMessage(chatId: String, text: String) {
        userProfileRepository.getUserProfile(currentUser.uid).subscribe { userProfile ->
            val messageInfo = MessageInfo.create(text, currentUser.uid)
            messageRepository.saveMessage(chatId, messageInfo)
        }
        viewState.clearMessageEditText()
    }

    override fun sendImage(chatId: String, uri: Uri) {
        messageRepository.saveImage(chatId, currentUser.uid, uri)
    }

    override fun openImagePickDialog() {
        viewState.openImagePickDialog()
    }

    override fun onMessageEditChanged(text: CharSequence) {
        val showOrHide = text.trim { it <= ' ' }.isNotEmpty()
        viewState.toggleSendButton(showOrHide)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun getReference(chatId: String): DatabaseReference =
            messageRepository.databaseReference(chatId)

    override fun initUserProfile(userId: String): Flowable<UserProfile> =
            userProfileRepository.observeUserProfile(userId)

    override fun getImageReference(imageUrl: String): StorageReference =
            messageRepository.getImageReference(imageUrl)

    override fun messagePublisher(chatId: String): FirebaseArray<Message> =
            chatInteractor.observeMessages(chatId)

    override fun onBackPressed() = router.exit()

    override fun onAvatarClicked(messageInfo: MessageInfo) {
        throw NotImplementedError()
    }

    override fun addToFavorites(chatId: String) {
        throw NotImplementedError()
    }

    override fun observeUserProfile(userId: String) =
            userProfileRepository.observeUserProfile(userId)

    companion object : KLogging()
}