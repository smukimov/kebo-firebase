package com.kebo.kebo.presentation.auth

import com.kebo.kebo.dagger.FragmentScoped
import com.kebo.kebo.ui.auth.CodeAuthFragment
import com.kebo.kebo.ui.auth.EmailAuthFragment
import com.kebo.kebo.ui.auth.PhoneAuthFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AuthModule {
    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun phoneAuthFragment(): PhoneAuthFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun codeAuthFragment(): CodeAuthFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun emailAuthFragment(): EmailAuthFragment
}