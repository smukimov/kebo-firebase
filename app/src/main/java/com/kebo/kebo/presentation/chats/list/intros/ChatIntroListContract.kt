package com.kebo.kebo.presentation.chats.list.intros

import com.arellomobile.mvp.MvpView
import com.firebase.ui.database.FirebaseIndexArray
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.models.interactors.HitInfo
import io.reactivex.Single

class ChatIntroListContract {
    interface View : MvpView {
        fun showNoCompanies()
    }

    interface Presenter {
        fun result(requestCode: Int, resultCode: Int)
        fun queryChat(term: CharSequence): Single<List<HitInfo>>
        fun onChatClicked(chatId: String)
        fun onHitClicked(hit: Any)
        fun createChat()
        fun observeCompanies(): FirebaseIndexArray<ChatGroupInfo>
        fun onBackPressed()
    }
}