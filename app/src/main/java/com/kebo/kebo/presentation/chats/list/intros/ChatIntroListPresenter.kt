package com.kebo.kebo.presentation.chats.list.intros

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.firebase.ui.database.FirebaseIndexArray
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.navigateTo
import com.kebo.kebo.models.interactors.HitInfo
import com.kebo.kebo.models.interactors.chats.ChatListInteractor
import com.kebo.kebo.models.interactors.ChatGroupInfo
import com.kebo.kebo.presentation.global.GlobalMenuController
import mu.KLogging
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ChatIntroListPresenter
    @Inject constructor(private val chatListInteractor: ChatListInteractor,
                        private val menuController: GlobalMenuController,
                        private val router: Router)
    : MvpPresenter<ChatIntroListContract.View>(), ChatIntroListContract.Presenter {

    override fun onHitClicked(hit: Any) {
        val hitInfo = hit as HitInfo
        router.navigateTo(ScreenType.CHAT_GROUP, hitInfo.hit.objectID as Any)
    }

    override fun createChat() {
        router.navigateTo(ScreenType.CHAT_CREATE)
    }

    override fun queryChat(term: CharSequence) =
            chatListInteractor.searchChatByTerm(term.toString())

    override fun result(requestCode: Int, resultCode: Int) {
        TODO("not implemented")
    }

    override fun onChatClicked(chatId: String) {
        router.navigateTo(ScreenType.CHAT_GROUP, chatId)
    }

    override fun onBackPressed() {
        router.exit()
    }

    override fun observeCompanies(): FirebaseIndexArray<ChatGroupInfo> =
            chatListInteractor.observeIntroList()

    fun onMenuPressed() {
        menuController.open()
    }

    companion object : KLogging()
}
