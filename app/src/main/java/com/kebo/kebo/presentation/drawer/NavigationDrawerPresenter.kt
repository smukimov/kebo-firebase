package com.kebo.kebo.presentation.drawer

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kebo.kebo.ScreenType
import com.kebo.kebo.extension.addTo
import com.kebo.kebo.extension.newRootScreen
import com.kebo.kebo.models.data.auth.AuthManager
import com.kebo.kebo.models.repository.UserProfileRepository
import com.kebo.kebo.presentation.drawer.NavigationDrawerContract.View.MenuItem
import com.kebo.kebo.presentation.drawer.NavigationDrawerContract.View.MenuItem.*
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class NavigationDrawerPresenter
    @Inject constructor(private val userProfileRepository: UserProfileRepository,
                        private val authManager: AuthManager,
                        private val router: Router)
    : MvpPresenter<NavigationDrawerContract.View>(), NavigationDrawerContract.Presenter {
    private var currentSelectedItem: MenuItem? = null
    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
    override fun showUserProfile() {
        userProfileRepository
                .observeUserProfile(authManager.currentUser.uid)
                .subscribe({ viewState.showUserInfo(it) }, {}).addTo(disposable)
    }

    override fun onScreenChanged(item: MenuItem) {
        currentSelectedItem = item
        viewState.selectMenuItem(item)
    }

    override fun onMenuItemClick(item: MenuItem) {
        if (item != currentSelectedItem) {
            when (item) {
                SETTINGS -> router.newRootScreen(ScreenType.SETTINGS)
                CHATS_INTRO -> router.newRootScreen(ScreenType.CHATS_INTRO)
                CHATS_PRIVATE -> router.newRootScreen(ScreenType.CHATS_PRIVATE)
                FAVORITES -> router.newRootScreen(ScreenType.CHATS_FAVORITES)
                SIGN_OUT -> signOut()
            }
        }
    }

    override fun signOut() {
        authManager.signOut()
    }
}