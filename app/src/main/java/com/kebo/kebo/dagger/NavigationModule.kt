package com.kebo.kebo.dagger

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Singleton
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Cicerone

@Module
class NavigationModule {
    private val cicerone: Cicerone<Router> = Cicerone.create()

    @Provides
    @Singleton
    internal fun provideRouter(): Router = cicerone.router

    @Provides
    @Singleton
    internal fun provideNavigatorHolder(): NavigatorHolder = cicerone.navigatorHolder
}