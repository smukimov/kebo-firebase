package com.kebo.kebo.dagger

import android.app.Application
import android.content.Context
import com.kebo.kebo.models.data.server.AlgoliaApi
import com.kebo.kebo.models.data.server.AlgoliaApiProvider
import com.kebo.kebo.models.data.server.FirebaseApi
import com.kebo.kebo.models.data.server.FirebaseApiProvider

import dagger.Module
import dagger.Binds
import dagger.Reusable
import javax.inject.Singleton

@Module
abstract class ApplicationModule(private val application: Application) {

    @Binds
    internal abstract fun bindContext(application: Application): Context

    @Binds
    @Reusable
    internal abstract fun bindFirebaseApi(firebaseApiProvider: FirebaseApiProvider): FirebaseApi

    @Binds
    @Reusable
    internal abstract fun bindAlgoliaApi(algoliaApiProvider: AlgoliaApiProvider): AlgoliaApi
}
