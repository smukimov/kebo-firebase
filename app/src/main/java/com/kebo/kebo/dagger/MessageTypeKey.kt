package com.kebo.kebo.dagger

import com.kebo.kebo.entities.MessageType
import dagger.MapKey

@MapKey
@Target(AnnotationTarget.FUNCTION)
annotation class MessageTypeKey(val value: MessageType)