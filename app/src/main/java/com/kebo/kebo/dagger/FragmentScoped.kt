package com.kebo.kebo.dagger

import javax.inject.Scope
import kotlin.annotation.Retention
import kotlin.annotation.Target

@Scope
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class FragmentScoped