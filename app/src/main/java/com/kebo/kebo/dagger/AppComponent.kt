package com.kebo.kebo.dagger

import dagger.Component
import dagger.android.DaggerApplication
import javax.inject.Singleton
import android.app.Application
import com.google.firebase.auth.FirebaseUser
import com.kebo.kebo.App
import com.kebo.kebo.presentation.global.GlobalMenuController
import dagger.BindsInstance
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 * This is a Dagger component. Refer to {@link App} for the list of Dagger components
 * used in this application.
 * <p>
 * Even though Dagger allows annotating a {@link Component} as a singleton, the code
 * itself must ensure only one instance of the class is created. This is done in {@link
 * App}.
 * //{@link AndroidSupportInjectionModule}
 * // is the module from Dagger.Android that helps with the generation
 * // and location of subComponents.
 */

@Singleton
@Component(modules = arrayOf(
        NavigationModule::class,
        FirebaseModule::class,
        ApplicationModule::class,
        ActivityBindingModule::class,
        AndroidSupportInjectionModule::class))
interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(application: App)

    override fun inject(instance: DaggerApplication)

    fun firebaseUser(): FirebaseUser?

    // Gives us syntactic sugar. we can then do
    // DaggerAppComponent.builder().application(this).build().inject(this);
    // never having to instantiate any modules or say which module we are passing the application to.
    // Application will just be provided into our app graph now.
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}