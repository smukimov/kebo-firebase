package com.kebo.kebo.dagger

import com.kebo.kebo.models.interactors.ChatInfoType
import dagger.MapKey

@MapKey
@Target(AnnotationTarget.FUNCTION)
annotation class ChatTypeKey(val value: ChatInfoType)