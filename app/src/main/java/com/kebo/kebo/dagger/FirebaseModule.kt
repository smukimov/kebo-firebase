package com.kebo.kebo.dagger

import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class FirebaseModule {
    @Provides
    @Singleton
    @Named("LogoStorage")
    fun provideLogosStorage(): FirebaseStorage =
            FirebaseStorage.getInstance("gs://kebo-93097-logos")

    @Provides
    @Singleton
    @Named("DefaultStorage")
    fun providerDefaultStorage(): FirebaseStorage = FirebaseStorage.getInstance()

    @Provides
    @Singleton
    fun provideFirebaseDatabase(): FirebaseDatabase {
        val firebaseDatabase = FirebaseDatabase.getInstance()
        firebaseDatabase.setPersistenceEnabled(true)
        return firebaseDatabase
    }

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Singleton
    fun providePhoneAuthProvider(): PhoneAuthProvider = PhoneAuthProvider.getInstance()

    @Provides
    @Singleton
    fun provideFirebaseAnalytics(context: Context): FirebaseAnalytics =
            FirebaseAnalytics.getInstance(context)

    @Module companion object {
        @JvmStatic @Provides
        fun currentUser(firebaseAuth: FirebaseAuth) = firebaseAuth.currentUser!!
    }
}