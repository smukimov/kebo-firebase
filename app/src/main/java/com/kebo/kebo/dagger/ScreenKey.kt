package com.kebo.kebo.dagger

import com.kebo.kebo.ScreenType
import dagger.MapKey

@MapKey
@Target(AnnotationTarget.FUNCTION)
annotation class ScreenKey(val value: ScreenType)