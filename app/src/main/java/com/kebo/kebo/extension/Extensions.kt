package com.kebo.kebo.extension

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kebo.kebo.ScreenType
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Router

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}

fun Router.navigateTo(screenType: ScreenType) {
    this.navigateTo(screenType.name)
}

fun Router.navigateTo(screenType: ScreenType, data: Any) {
    this.navigateTo(screenType.name, data)
}

fun Router.newRootScreen(screenType: ScreenType) {
    this.newRootScreen(screenType.name)
}